---
title: Activities
subtitle: My data science related activities and achievements
description: My data science related activities, talks and achievements
featured_image: /images/49007498_2251964874823621_1243817482335027200_o.jpg
---
<div class="gallery" data-columns="4">
    <img src="https://road-to-kaggle-grandmaster.vercel.app/api/badges/artgor/competition/light" width="5%" />
    <img src="https://road-to-kaggle-grandmaster.vercel.app/api/badges/artgor/dataset/light" width="5%" />
    <img src="https://road-to-kaggle-grandmaster.vercel.app/api/badges/artgor/notebook/light" width="5%" />
    <img src="https://road-to-kaggle-grandmaster.vercel.app/api/badges/artgor/discussion/light" width="5%"/>
</div>

<details>
<summary>My talks</summary>

<p><em>2022:</em></p>
<ul>
<li><a href="https://www.youtube.com/watch?v=svyV44fTinQ&ab_channel=sBerlogabig">Уроки извлеченные из 10 лет в IT (ERP и DS) (talk in Russian)</a></li>
<li><a href="https://youtu.be/JbcmV-G_K7k">Video Super-Resolution: Approaches to modelling and evaluation.</a></li>

</ul>


<p><em>2021:</em></p>
<ul>
<li><a href="https://youtu.be/pjvJoGwXVXU">Из бизнеса в Data Science, из DS в DL. Андрей Лукьяненко о своих победах</a></li>
<li><a href="https://youtu.be/sbrAY1TObmc">Data Visualization in Data Science (Russian version).</a></li>
<li><a href="https://youtu.be/Hez-1kLo_Yc">Data Visualization in Data Science.</a></li>
<li><a href="https://youtu.be/WGZpsDSmHnE">Practical aspects of working with NER task (talk in the Russian language).</a></li>
<li><a href="https://www.youtube.com/watch?v=J_HM8WctaBU&list=PLlxErbAvYYLDRP6cHtVP76f2g5Yoh6c5R&index=14">Writing reusable training pipelines for deep learning. ML REPA.</a></li>
<li><a href="https://youtu.be/T_UyGSUUDKc">A talk in the Russian language about using Hydra framework for configuration in Python projects.</a></li>
<li><a href="https://www.youtube.com/watch?v=n5aZBEnUHxc&amp;feature=youtu.be&amp;ab_channel=VishwasNarayan">Podcast talk about Kaggle, modern ML technologies, frameworks, hardware, and many other things</a></li>
<li><a href="https://www.youtube.com/watch?v=-yfN7kMZj9I&amp;feature=youtu.be&amp;ab_channel=SetuChokshi">Building your Data Science Career: how to become a data scientist</a></li>
</ul>

<p><em>2020:</em></p>
<ul>
<li><a href="https://www.youtube.com/watch?v=PX1RoRD3o7o">Andrey Lukyanenko - Handwritten digit recognition w/ a twist &amp; topic modelling over time</a></li>
<li><a href="https://www.youtube.com/watch?v=VRVit0-0AXE">Pair Programming: Deep Learning Model For Drug Classification With Andrey Lukyanenko</a></li>
<li><a href="https://youtu.be/0U2Mj0MDa20">DataFest 2020 talk: My Kaggle Story</a></li>
<li><a href="https://youtu.be/D7i67UT3O3o">Overview of approaches to &quot;Mechanisms of Action (MoA) Prediction&quot; competition on Kaggle (3 weeks before the end of the competition)</a></li>
<li><a href="https://www.youtube.com/watch?v=yxtUnF78bBE&amp;feature=emb_logo&amp;ab_channel=ODSAIGlobal">Metro Analysis and Visualization</a></li>
<li><a href="https://www.youtube.com/watch?v=ZBG_lvhQm4g&amp;feature=youtu.be&amp;ab_channel=MindhackSummit">Writing reusable pipelines in Deep Learning | Mindhack! Summit</a></li>
<li><a href="https://youtu.be/MjURy6Ow5D8?t=1800">December Lightning Talks. Training pipeline with Pytorch Lightning and Hydra</a></li>
</ul>

<p><em>2019:</em></p>
<ul>
<li><a href="https://www.youtube.com/watch?v=BXjSd3OxyDM">ML Training talks about gold winning solution in Kaggle Predicting Molecular Properties Competition</a></li>
<li><a href="https://www.youtube.com/watch?v=LQhlZLTn84g">An overview of 2019 year in Kaggle, Talk in Russian at ODS.AI event</a></li>
<li><a href="https://www.youtube.com/watch?v=hoo-VZ3Rd7Y">Data Halloween: predicting chaotic actions of clients. Talk in Russian at ODS.AI event</a></li>
<li><a href="https://www.youtube.com/watch?v=fwnHN5Dsivo">Storytelling with data visualization: case of kaggle kernels. Talk in Russian at Data Fest 2019</a></li>

</ul>

</details>

<details>
<summary>My interviews</summary>

<ul>
<li><a href="https://youtu.be/pjvJoGwXVXU">Из бизнеса в Data Science, из DS в DL. Андрей Лукьяненко и своих победах</a></li>
<li><a href="https://www.youtube.com/watch?v=rpClh8WmTdo">Interview with Kaggle Kernels Grandmaster #1: Artgor | Andrew Lukyanenko. Chai Time Data Science</a> | <a href="https://player.fm/series/chai-time-data-science/interview-with-kaggle-kernels-grandmaster-1-artgor-andrew-lukyanenko">player.fm link</a></li>
<li><a href="https://www.machinecommons.org/post/kaggle-people-we-ve-gamified-progress">Kaggle: People, We’ve Gamified Progress</a></li>
<li><a href="https://www.analyticsvidhya.com/blog/2020/12/exclusive-interview-with-andrey-lukyanenko/">Kaggle Grandmaster Series – Exclusive Interview with Andrey Lukyanenko (Notebooks and Discussions Grandmaster)</a></li>
</ul>


</details>


<details>
<summary>ML competitions</summary>

<details>
<summary>Kaggle ranks</summary>

<p>Oct 10, 2019 - Discussion Grandmaster</p>
<p>Aug 29, 2019 - Competition Master</p>
<p>May 08, 2019 - 1st place in Notebook ranking</p>
<p>Feb 21, 2019 - Notebook Grandmaster</p>

</details>

<details>
<summary>Notable competitions</summary>

<p>Aug 29, 2019 - 8/2737. Gold medal in <a href="https://www.kaggle.com/c/champs-scalar-coupling/leaderboard">Predicting Molecular Properties</a></p>
<p>Aug 30, 2018 - 64/7176. Silver medal in <a href="https://www.kaggle.com/c/home-credit-default-risk/leaderboard">Home Credit Default Risk</a></p>
<p>Aug 21, 2018 - 108/4464. Silver medal in <a href="https://www.kaggle.com/c/santander-value-prediction-challenge/leaderboard">Santander Value Prediction Challenge</a></p>
<p>Oct 20, 2018 - 100/3219. Silver medal in <a href="https://www.kaggle.com/c/tgs-salt-identification-challenge/leaderboard">TGS Salt Identification Challenge</a></p>
<p>Mar 14, 2019 - 120/2410. Silver medal in <a href="https://www.kaggle.com/c/microsoft-malware-prediction/leaderboard">Microsoft Malware Prediction</a></p>
<p>Aug 28, 2019 - 87/921. Bronze medal in <a href="https://www.kaggle.com/c/generative-dog-images/leaderboard">Generative Dog Images</a></p>
<hr>
<p>May 09, 2019 - 23/58 <a href="https://ods.ai/competitions/idrnd-facial-antispoofing/leaderboard">Anti-spoofing challenge</a></p>
<p>Mar 30, 2018 - 25/92 <a href="https://ods.ai/competitions/mts-geohack-112/leaderboard">Geohack competition</a></p>
<p>Apr 24, 2018 - 3/67 <a href="https://www.kaggle.com/c/receipt-categorisation/leaderboard">Shop check categorization</a></p>


</details>

<details>
<summary>Notable kaggle notebooks</summary>

<h4 id="-eda-and-models-https-www-kaggle-com-artgor-eda-and-models-"><a href="https://www.kaggle.com/artgor/eda-and-models">EDA and models</a></h4>
<p>1000+ upvotes, 100k+ views. A notebook for <a href="https://www.kaggle.com/c/ieee-fraud-detection">IEEE-CIS Fraud Detection</a> competition. EDA, feature engineering, plots with altair, modelling with LGBM.</p>
<h4 id="-eda-feature-engineering-and-everything-https-www-kaggle-com-artgor-eda-feature-engineering-and-everything-"><a href="https://www.kaggle.com/artgor/eda-feature-engineering-and-everything">EDA, feature engineering and everything</a></h4>
<p>900+ upvotes, 110k+ views. A notebook for <a href="https://www.kaggle.com/c/two-sigma-financial-news">Two Sigma: Using News to Predict Stock Movements</a> competition. Text and tabular data EDA, analysis of stock market, plots with plotly, modelling with LGBM.</p>
<h4 id="-segmentation-in-pytorch-using-convenient-tools-https-www-kaggle-com-artgor-segmentation-in-pytorch-using-convenient-tools-"><a href="https://www.kaggle.com/artgor/segmentation-in-pytorch-using-convenient-tools">Segmentation in PyTorch using convenient tools</a></h4>
<p>600+ upvotes, 60k+ views. A notebook for <a href="https://www.kaggle.com/c/understanding_cloud_organization">Understanding Clouds from Satellite Images</a> competition. EDA, augmentations with albumentations, image segmentation using Catalyst.</p>
<h4 id="-exploration-of-data-step-by-step-https-www-kaggle-com-artgor-exploration-of-data-step-by-step-"><a href="https://www.kaggle.com/artgor/exploration-of-data-step-by-step">Exploration of data step by step</a></h4>
<p>600+ upvotes, 30k+ views. A notebook for <a href="https://www.kaggle.com/c/petfinder-adoption-prediction">PetFinder.my Adoption Prediction</a> competition. A very detailed EDA of tabular and text data, complex plots with matplotlib and seaborn, modelling with LGBM.</p>
<h4 id="-how-to-not-overfit-https-www-kaggle-com-artgor-how-to-not-overfit-"><a href="https://www.kaggle.com/artgor/how-to-not-overfit">How to not overfit?</a></h4>
<p>500+ upvotes, 30k+ views. A notebook for <a href="https://www.kaggle.com/c/dont-overfit-ii">Don&#39;t Overfit! II</a> competition. Model interpretation, comparison of many models, comparison of feature selection methods, approaches to feature engineering on anonymized data.</p>
<h4 id="-eda-feature-engineering-and-model-interpretation-https-www-kaggle-com-artgor-eda-feature-engineering-and-model-interpretation-"><a href="https://www.kaggle.com/artgor/eda-feature-engineering-and-model-interpretation">EDA, Feature Engineering and model interpretation</a></h4>
<p>400+ upvotes, 40k+ views. A notebook for <a href="https://www.kaggle.com/c/tmdb-box-office-prediction">TMDB Box Office Prediction</a> playground competition. A very detailes EDA for various types of data, advanced approaches to feature engineering, model interpretation.</p>
<h4 id="-eda-feature-engineering-and-xgb-lgb-https-www-kaggle-com-artgor-eda-feature-engineering-and-xgb-lgb-"><a href="https://www.kaggle.com/artgor/eda-feature-engineering-and-xgb-lgb">EDA, feature engineering and xgb + lgb</a></h4>
<p>100+ upvotes, 8k+ views. A notebook for <a href="https://www.kaggle.com/c/donorschoose-application-screening">DonorsChoose.org Application Screening</a> competition. This notebook won a second place in popularity ranking and got me a prize - <a href="https://www.kaggle.com/c/donorschoose-application-screening/discussion/55396">Pixelbook</a>.</p>

</details>

</details>


<details>
<summary>Notable courses completed</summary>

<h4 id="-mlcourse-ai-https-mlcourse-ai-"><a href="https://mlcourse.ai/">Mlcourse.ai</a></h4>
<p>Finished at <a href="https://github.com/Yorko/mlcourse.ai/wiki/Session-2-final-rating-(in-Russian)">5th place</a>. This was a useful course with useful theoretical materials and practical exercises.</p>
<h3 id="-deep-learning-nanodegree-https-www-udacity-com-course-deep-learning-nanodegree-nd101-with-pytorch-on-udacity-"><a href="https://www.udacity.com/course/deep-learning-nanodegree--nd101">Deep Learning Nanodegree</a> with Pytorch on Udacity.</h3>
<p><img src="https://miro.medium.com/max/2400/1*H07Sp5kyQCLB3sOP_xzhqg.png" alt="Certificate"></p>
<p>You can read about my experience of going through this course <a href="https://medium.datadriveninvestor.com/pytorch-deep-learning-nanodegree-introduction-161817c22384">here</a>.</p>

</details>

<details>
<summary>Other activities and achievements</summary>

<p><a href="https://datafest.ru/6/team/">Moscow DataFest 6 (2019)</a> - organized &quot;EDA &amp; Visualization&quot; track.</p>
<p><a href="https://old.ods.ai/awards/2019/">Open Data Science Awards 2019</a> Got awards for one on best talks in ML Trainings and for progress in competitions.</p>
<p><a href="https://fest.ai/2020/">Moscow DataFest 2020</a> - one of organizers of &quot;ML trainings&quot; track.</p>
<p><a href="https://ods.ai/events/elka2020">Open Data Science Awards 2020</a> Gor awards for one of best talks in Business section and for mentoring.</p>
<p>Completed Hacktoberfest in 2018 and 2020.</p>
<p><a href="https://ds.underhood.club/AndLukyane">Wrote</a> for a week in <a href="https://twitter.com/dsunderhood">dsunderhood</a></p>
<p>I'm one of the main contributors of an open-source <a href="https://ods.ai/projects/news-viz">project</a> for a topic modelling on news articles</p>
</details>
