---
title: About me
subtitle: Some info about me
description: Some info about me 
featured_image: /images/demo/about.jpg
---
<img align="right" src="https://andlukyane.com/images/2020-05-11_18-06-02.jpg" width="40%" style="margin:0px 10px">


I'm Andrey Lukynenko, data scientist, economist, and polyglot.
I was born and still live in Moscow, Russia.
My formal education as an economist didn't include any programming or machine learning, so as a result, I was more focused on analytical thinking and learning new things.

#### ERP 
Several years of my early career were spent as an ERP-consultant (SAP, Галактика), I have worked with people from different industries (oil and gas, transportation, architecture and more) and professions (accounting, contracts, logistics, IT, budgeting and more). It was a fascinating experience, which taught me a lot about how businesses work, but I ultimately realized that I wanted to do something different in the future.

#### Data Science

After months of searching and thinking, I have decided to try working as a data scientist. I have left my last job in consulting and started studying programming, statistics and math, machine learning itself, and since April 2017, I have begun working as a data scientist.

I have worked in telecom, banking, and various startups. Some examples of the projects are churn prediction, fraud detection, various analytics, text video analytics, chat-bots, video super-resolution, and many more.

#### Activities

In my free time, I pursue various activities to get more knowledge and experience:
* Take part in competitions (mostly Kaggle).
* Do pet-projects.
* Contribute to open-source and volunteer.
* Help people solve their problems.
* Give talks in conferences and even take part in their organization.


Before switching my career, I have spent a lot of time studying languages - German, Spanish, Japanese.

Also, I like reading fantasy; some of my favorite authors are [John Bierce](https://www.goodreads.com/author/show/18510329.John_Bierce), [Brandon Sanderson](https://www.goodreads.com/author/show/38550.Brandon_Sanderson), [Will Wight](https://www.goodreads.com/author/show/7125278.Will_Wight).

----------------

#### Random facts

* I'm an economist by education
* I have read "War and Peace" three times
* At some points in my life, I have studied taekwondo, fencing, and chess
* I have Lean Six Sigma Green Belt [certificate](https://i.imgur.com/KY7uSrd.jpg)
* I have more than 80k total XP on [Duolingo](https://www.duolingo.com/profile/Artgor) 
* I passed [B2](https://1.bp.blogspot.com/-0ucSixsyG0g/Ukf176E0j9I/AAAAAAAAAOA/IvI1-Y4UVOA/s1600/29-09-2013+13-38-13.jpg) for German on Deutsche Welle in 2013
* I have an old [blog](https://erlemar.blogspot.com/), which is mostly about learning languages
* I passed [JLPT3](https://2.bp.blogspot.com/-D86c4jHCzHI/VAL6bkxXTrI/AAAAAAAAAXE/C_TczY1YRB4/s1600/2014-08-31%2B14-34-04.jpg) in J-CAT test in August 2014
* I'm one of the authors of a telegram channel [Data Science by ODS.ai](https://t.me/opendatascience)
* I have spent a lot of time on [origami](https://sun9-3.userapi.com/c1870/u51847793/96762536/x_ea0082d2.jpg)

