---
title: My working experience
subtitle: Companies I worked at
description: Companies I worked at 
featured_image: /images/84624834_1754285441362655_1253283272427831296_o.jpg
---
You can get a pdf file with my CV [here]({{site.url}}resume.pdf)
<section id="cd-timeline" class="cd-container">
		<div class="cd-timeline-block">
			<div class="cd-timeline-img cd-movie">
				<img src="https://upload.wikimedia.org/wikipedia/en/e/e3/Careem_logo.svg" alt="Careem">
			</div>
			<div class="cd-timeline-content">
				<h2>Senior Data Scientist at Careem</h2>
				<details>
<summary>Anti-fraud projects</summary>
<p><ul>
<li style="font-size:16px">Worked on pre-autorization project end-to-end;</li>
</ul></p>
</details>
				<span class="cd-date">May 9, 2022</span>
			</div> <!-- cd-timeline-content -->
		</div> <!-- cd-timeline-block -->
<div class="cd-timeline-block">
			<div class="cd-timeline-img cd-picture">
				<img src="https://upload.wikimedia.org/wikipedia/commons/8/86/MTS_logo_2015.svg" alt="MTS AI">
			</div>
			<div class="cd-timeline-content">
				<h2>ML Engineer / TechLead at MTS AI</h2>
<details>
<summary>Medical chat-bot project</summary>
<p><ul>
<li style="font-size:16px">improved data collection approaches for labeling using active learning, Snorkel, and pseudo-labeling;</li>
<li style="font-size:16px">developed an approach to check the quality of text labeling, which improved the speed of analysis and helped to find errors in labels;</li>
<li style="font-size:16px">trained ML models for text classification and NER;</li>
<li style="font-size:16px">experimented with various approaches to improving model quality, including text augmentations, modifications to model architectures, different loss functions;</li>
<li style="font-size:16px">developed a training pipeline based on PyTorch-Lightning and Hydra;</li>
<li style="font-size:16px">introduced principles of writing good code including texts, style checking, autoformatting;</li>
<li style="font-size:16px">mentored colleagues;</li>
<li style="font-size:16px">organized weekly meetups to share expertise;</li>
</ul></p>
</details>

<details>
<summary>Video enhancement project</summary>
<p><ul>
<li style="font-size:16px">Used video super-resolution approaches to improve video quality;</li>
</ul></p>
</details>

<details>
<summary>Leading CV R&D team</summary>
<p><ul>
<li style="font-size:16px">managing research;</li>
<li style="font-size:16px">researching self-supervision approaches for training GANs;</li>
<li style="font-size:16px">experiments with hardware (jetson, kneron);</li>
</ul></p>
</details>
				<span class="cd-date">Nov 14, 2019</span>
			</div>
		</div>
		<div class="cd-timeline-block">
			<div class="cd-timeline-img cd-movie">
				<img src="https://upload.wikimedia.org/wikipedia/commons/6/69/Tele2_logo.svg" alt="Tele2">
			</div>
			<div class="cd-timeline-content">
				<h2>Senior DS for BigData Research, Tele2</h2>
				<details>
<summary>BigData projects</summary>
<p><ul>
<li style="font-size:16px">Implemented churn prediction, fraud detection, and look-alike models;</li>
<li style="font-size:16px">Developed and implemented an algorithm predicting the actual value of all client devices;</li>
<li style="font-size:16px">Took part in the organization of two hackathons;</li>
<li style="font-size:16px">Completed several geo-analytics projects;</li>
<li style="font-size:16px">Worked on projects improving quality of customers’ service by call-center;</li>
<li style="font-size:16px">Developed a prototype of human detection on video and predicting age/gender;</li>
</ul></p>
</details>
				<span class="cd-date">May 7, 2018</span>
			</div> <!-- cd-timeline-content -->
		</div> <!-- cd-timeline-block -->
		<div class="cd-timeline-block">
			<div class="cd-timeline-img cd-picture">
				<img src="https://media-exp1.licdn.com/dms/image/C4E0BAQEuk-8iCl8mcw/company-logo_200_200/0/1519875246045?e=2159024400&v=beta&t=kU3Z8EdZahH1MHyLPaQYvLLKA2LOWLhTHTk2VeKvHa4" alt="Maxima Telecom">
			</div> <!-- cd-timeline-img -->
			<div class="cd-timeline-content">
				<h2>Data Scientist, Maxima Telecom</h2>
				
<ul>
<li style="font-size:16px">Developed algorithm for predicting home and work station for users based on the logs of their activity;</li>
<li style="font-size:16px">Developed algorithm for predicting user interests based on the visited sites;</li>
</ul>
				<span class="cd-date">Nov 20, 2017</span>
			</div> <!-- cd-timeline-content -->
		</div> <!-- cd-timeline-block -->
		<div class="cd-timeline-block">
			<div class="cd-timeline-img cd-picture">
				<img src="https://media-exp1.licdn.com/dms/image/C510BAQF3boO0b5qr3g/company-logo_200_200/0/1519885911029?e=2159024400&v=beta&t=tIADXaFHHmIwOWthvKy6gtx_iQ96WxJoWC00w3Xln7g" alt="Frumatic">
			</div> <!-- cd-timeline-img -->
			<div class="cd-timeline-content">
				<h2>Data Scientist, Frumatic</h2>
				<p>Developed recommendation system using NLP</p>
				<span class="cd-date">Sep 06, 2017</span>
			</div> <!-- cd-timeline-content -->
		</div> <!-- cd-timeline-block -->
		<div class="cd-timeline-block">
			<div class="cd-timeline-img cd-location">
				<img src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Otp_bank_Logo.svg" alt="OTP Bank">
			</div> <!-- cd-timeline-img -->
			<div class="cd-timeline-content">
				<h2>Data Scientist, OTP Bank</h2>
				<p> Developed the full scope of data science pipeline and finalized a project for predicting a probability of customer’s positive answer to bank’s cross-sell offer. The model was approved to be implemented in production.</p>
				<span class="cd-date">Apr 17, 2017</span>
			</div> <!-- cd-timeline-content -->
		</div> <!-- cd-timeline-block -->
		<div class="cd-timeline-block">
			<div class="cd-timeline-img cd-location">
				<img src="{{site.url}}images/various/erp.svg" alt="Location">
			</div> <!-- cd-timeline-img -->
			<div class="cd-timeline-content">
				<h2>Erp-consultant in various companies</h2>
				<p>  
<ul>
<li style="font-size:16px">Worked in three consulting companies working in ERP-system implementation and support;</li>
<li style="font-size:16px">Successfully led a project on building a web analytical reporting system from the design stage until its delivery to the client;</li>
<li style="font-size:16px">Formalized project concept and technical documentation for a budgeting system;</li>
<li style="font-size:16px">Curated the development accounting, logistics, and contract managing modules;</li>
</ul>
</p>
				<span class="cd-date">Jul 02, 2012</span>
			</div> <!-- cd-timeline-content -->
		</div> <!-- cd-timeline-block -->
</section>