---
title: About me
subtitle: Some info about me
description: Some info about me 
featured_image: /images/demo/about.jpg
---

<p><img align="right" src="https://andlukyane.com/images/2020-05-11_18-06-02.jpg" width="40%" style="margin:0px 10px" /></p>

<p>I&#39;m Andrey Lukynenko, data scientist, economist, and polyglot.
I was born and still live in Moscow, Russia.
My formal education as an economist didn&#39;t include any programming or machine learning, so as a result, I was more focused on analytical thinking and learning new things.</p>
<h4 id="erp">ERP</h4>
<p>Several years of my early career were spent as an ERP-consultant (SAP, Галактика), I have worked with people from different industries (oil and gas, transportation, architecture and more) and professions (accounting, contracts, logistics, IT, budgeting and more). It was a fascinating experience, which taught me a lot about how businesses work, but I ultimately realized that I wanted to do something different in the future.</p>
<h4 id="data-science">Data Science</h4>
<p>After months of searching and thinking, I have decided to try working as a data scientist. I have left my last job in consulting and started studying programming, statistics and math, machine learning itself, and since April 2017, I have begun working as a data scientist.</p>
<p>I have worked in telecom, banking, and various startups. Some examples of the projects are churn prediction, fraud detection, various analytics, text video analytics, chat-bots, video super-resolution, and many more.</p>

<h4 id="activities">Activities</h4>
<p>In my free time, I pursue various activities to get more knowledge and experience:</p>
<ul>
<li>Take part in competitions (<span style="color:red">Kaggle Competition Master</span> and <span style="color:darkorange">Kaggle Notebooks 1st rank</span>).</li>
<li>Do pet-projects.</li>
<li>Contribute to open-source and volunteer.</li>
<li>Help people solve their problems. Got an award from ODS.ai for mentorship (2020).</li>
<li>Give talks in conferences and even take part in their organization. Got an award from ODS.ai for one of best talks in 2019.</li>
</ul>
<p>Before switching my career, I have spent a lot of time studying languages - German, Spanish, Japanese.</p>
<p>Also, I like reading fantasy; some of my favorite authors are <a href="https://www.goodreads.com/author/show/18510329.John_Bierce">John Bierce</a>, <a href="https://www.goodreads.com/author/show/38550.Brandon_Sanderson">Brandon Sanderson</a>, <a href="https://www.goodreads.com/author/show/7125278.Will_Wight">Will Wight</a>.</p>


<h4 id="rf">Random facts</h4>

<ul>
  <li>I’m an economist by education</li>
  <li>I have read “War and Peace” three times</li>
  <li>At some points in my life, I have studied taekwondo, fencing, and chess</li>
  <li>I have Lean Six Sigma Green Belt <a href="https://i.imgur.com/KY7uSrd.jpg">certificate</a></li>
  <li>I have more than 80k total XP on <a href="https://www.duolingo.com/profile/Artgor">Duolingo</a></li>
  <li>I passed <a href="https://1.bp.blogspot.com/-0ucSixsyG0g/Ukf176E0j9I/AAAAAAAAAOA/IvI1-Y4UVOA/s1600/29-09-2013+13-38-13.jpg">B2</a> for German on Deutsche Welle in 2013</li>
  <li>I have an old <a href="https://erlemar.blogspot.com/">blog</a>, which is mostly about learning languages</li>
  <li>I passed <a href="https://2.bp.blogspot.com/-D86c4jHCzHI/VAL6bkxXTrI/AAAAAAAAAXE/C_TczY1YRB4/s1600/2014-08-31%2B14-34-04.jpg">JLPT3</a> in J-CAT test in August 2014</li>
  <li>I’m one of the authors of a telegram channel <a href="https://t.me/opendatascience">Data Science by ODS.ai</a></li>
  <li>I have spent a lot of time on <a href="https://sun9-3.userapi.com/c1870/u51847793/96762536/x_ea0082d2.jpg">origami</a></li>
</ul>