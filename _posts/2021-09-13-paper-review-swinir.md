---
title: 'Paper Review: SwinIR Image Restoration Using Swin Transformer'
date: 2021-09-13 00:00:00
description: My review of the paper SwinIR Image Restoration Using Swin Transformer
featured_image: 'https://andlukyane.com/images/paper_reviews/swinir/2021-09-13_13-47-09.jpg'
tags: paperreview deeplearning cv transformer superresolution imagerestoration
---

## Paper Review: SwinIR: Image Restoration Using Swin Transformer

[Paper link](https://arxiv.org/abs/2108.10257)

[Code link](https://github.com/JingyunLiang/SwinIR)

![Main image](https://andlukyane.com/images/paper_reviews/swinir/2021-09-13_13-47-09.jpg)

Image restoration is a vision problem that aims to restore high-quality images from low-quality images (e.g., downscaled, noisy, and compressed images).

The authors use a model based on the Swin Transformers. Experimental results demonstrate that SwinIR outperforms state-of-the-art methods on different tasks (image super-resolution, image denoising, and JPEG compression artifact reduction) by up to 0.14∼0.45dB, while the total number of parameters can be reduced by up to 67%.

-------

## The approach

Compared with widely used CNN-based image restoration models, transformer-based SwinIR has several benefits:
* content-based interactions between image content and attention weights, which can be interpreted as spatially varying convolution;
* long-range dependency modeling is enabled by the shifted window mechanism;
* better performance with fewer parameters;

## The architecture

![Architecture](https://andlukyane.com/images/paper_reviews/swinir/2021-09-13_13-39-50.jpg)

SwinIR consists of three modules: shallow feature extraction, deep feature extraction, and high-quality image reconstruction modules:

* **Shallow feature extraction** module uses a convolution layer (3x3) to extract shallow features that are fed into both following layers;
* **Deep feature extraction module** is composed of K residual Swin Transformer blocks (RSTB), each of which utilizes several Swin Transformer layers for local attention and cross-window interaction. The authors add a convolutional layer at the end of the block for feature enhancement and use a residual connection to provide a shortcut for feature aggregation;
* Both shallow and deep features are fused in the **reconstruction module** for high-quality image reconstruction. Different tasks use different reconstruction modules. For super-resolution, a sub-pixel convolutional layer is used. For other tasks, they use a single convolutional layer. Besides, they use residual learning to reconstruct a residual between LQ and HQ images instead of the HQ image;

![Reconstruction](https://andlukyane.com/images/paper_reviews/swinir/2021-09-13_13-41-23.jpg)

### Losses

* For classical and lightweight image SR the loss is a simple L1;
* For real-world SR, they use a combination of pixel loss, GAN loss, and perceptual loss;
* For image denoising and JPEG compression artifact reduction, they use the Charbonnier loss;

![Charbonnier](https://andlukyane.com/images/paper_reviews/swinir/2021-09-13_13-42-09.jpg)


## Results

Better and smaller models for all tasks.

<div class="gallery" data-columns="6">
	<img src="{{site.url}}images/paper_reviews/swinir/2021-09-13_13-43-05.jpg">
	<img src="{{site.url}}images/paper_reviews/swinir/2021-09-13_13-43-35.jpg">
	<img src="{{site.url}}images/paper_reviews/swinir/2021-09-13_13-44-00.jpg">
	<img src="{{site.url}}images/paper_reviews/swinir/2021-09-13_13-44-20.jpg">
	<img src="{{site.url}}images/paper_reviews/swinir/2021-09-13_13-44-35.jpg">
	<img src="{{site.url}}images/paper_reviews/swinir/2021-09-13_13-45-24.jpg">
</div>

## Ablations

<div class="gallery" data-columns="2">
	<img src="{{site.url}}images/paper_reviews/swinir/2021-09-13_13-46-30.jpg">
	<img src="{{site.url}}images/paper_reviews/swinir/2021-09-13_13-46-51.jpg">
</div>

* The higher the channel number, RSTB number, and STL number in a RSTB, the better;
* The bigger the image patches, the better;
* Converges better and faster than CNN models;
