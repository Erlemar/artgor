---
title: 'How I relocated and started living in Dubai'
date: 2022-08-26 00:00:00
description: A story of relocation to Dubai and of living there for several months
featured_image: 'https://andlukyane.com/images/dubai_relocation_2022/2022-08-25_18-00-13.jpg'
tags: blogpost career life 
---

## How I relocated and started living in Dubai

![Main image](https://andlukyane.com/images/dubai_relocation_2022/2022-08-25_18-00-13.jpg)

For a long time, I thought it would be nice to try living and working in another country, but I didn't actively try to do it until recently. The situation changed on 24th February 2022, and I decided it was time to leave Russia for some time. Luckily then, I had already started interviewing with a company located in Dubai and could move there quite quickly. This blog post is about my experience of moving to Dubai and living there for several months.

## Interviewing and getting the new job

![Careem](https://andlukyane.com/images/dubai_relocation_2022/careem-white-wink.jpeg)

In the middle of February, I was contacted by a recruiter from Careem. I was offered an opportunity to interview for the position of senior data scientist with a choice of location in Berlin, Dubai, or remote. I decided to try it and see how it goes.

The first stage was an online test on Hackerrank. I had 1 hour to answer 14 test questions about statistics, programming, and ML and to solve a small programming task. Then there were three more technical interviews: programming/algorithms, mathematics/statistics, and ML/system design. The last two interviews were behavioral: hiring manager interview and bar raiser. To prepare for them, I had to read the presentations and other materials about Careem's culture and values and discuss how my experience was relevant to them.

I successfully passed all the interviews and got a job offer. Unfortunately, this was in March, so the possibility of working remotely from Russia was unfeasible. The recruiter told me that the company couldn't relocate a Russian to Berlin at the moment. This left me with a single option - moving to Dubai. I decided that it would be a unique and exciting opportunity and agreed.

The relocation process itself turned out to be much longer and more complicated.

## Moving to a different country

I needed to do a lot of things before leaving Russia.

First of all, it turned out that my foreign passport ended half a year ago. Usually, getting a new passport isn't a problem - you just fill in a form on an official website, submit biometric data, and get a new document within several weeks. But due to a sudden influx of people trying to get or change their passports, there were long queues, and people had to wait for two or months to complete the process. As usual, this meant that the shady business of getting an official passport for a "special payment" was booming. One could find a lot of offers to produce a fully official foreign passport in 1-2 days for a significant payment. 

Another critical question was about vaccination against coronavirus. I was vaccinated - at first with Sputnik V, then with Sputnik Lite, but half a year passed since the last dose, so I decided to do it again. Unfortunately, I didn't calculate the dates correctly, so I got the second dose of Sputnik V only one day before my flight.

Preparing other documents was easy, well it was supposed to be easy. I collected most of the papers without difficulties, but an attested degree was an exception. I had a Bachelor's and Master's degree from Moscow State University. The Master's degree was in Russian and English, but this wasn't enough for UAE. If I wanted my diploma to be accepted there, it was necessary to make it attested - translate it into Arabic, get it attested, and go with it to two Russian Ministries and to the UAE embassy. There are companies that complete all the steps end-to-end, and usually, it takes ~3 weeks. But the process became very long due to the massive number of people who wanted to do this. And there was Ramadan, so the embassy didn't work full hours. So I waited for two months to get my degree attested and received the documents when I was already in Dubai.
The thing is - an attested degree turned out to be not necessary; I just got a visa like a lower-skilled professional.

One more difficulty was about getting money that I could use in Dubai before receiving the first salary:
* I had a Russian Mastercard debit card, but due to the sanctions, it wouldn't function in Dubai;
* Payment system MIR didn't work well in Dubai - some people said that it worked in some places, but it was too unreliable;
* I couldn't just get money from my accounts in dollars - there was a new restriction by the Russian government that if you didn't have a dollar/euro account opened before 9th of March 2022, you couldn't get foreign currency from the banks;

There were some options that I wasn't comfortable with: using a gray market and exchanging currency illegally or traveling to Dubai and exchanging cash with some person there - both were too risky.

I found out there were banks in Russia issuing cards with the UnionPay payment system and tried this approach. It worked, though it took too much time (I even had to move my traveling date forward). And later, I found several currency exchange points and got dollars there.

![Dollars](https://andlukyane.com/images/dubai_relocation_2022/20220425_112349.jpeg)

Another thing to do was to book an air ticket and a hotel. Fortunately, the company provided a relocation allowance, so they booked me a hotel and deducted the cost from my allowance. However, booking a ticket was more complicated as it was difficult to choose and approve the options. In the end, I booked the ticket on the Flydubai website by myself, called tech support, and got a "magic" link that allowed me to pay for the ticket using a Russian MasterCard.

My relatives decided to stay in Moscow, so I didn't have to do anything about managing the apartment but instead had to complete some tasks that were long overdue or buy some things to replace the old ones. And I had to teach my relatives some technical stuff I usually did by myself.

I spent much time looking up information about Dubai to be better prepared, joined telegram groups, etc. Some of the discovered things were useful and relevant, and some weren't helpful at all.

At that point, there were many cases when people were interrogated at the border, their smartphones were searched for restricted information, and sometimes people weren't allowed to leave. I spent some time preparing for it, but everything turned out to be very simple - I was asked about the amount of cash I had (a limit of 10000 $ per person) and whether I was going on a vacation. That's all.

## First days in Dubai

I arrived early in the morning, but the temperature was already boiling compared to Russia. At the airport, I got a stamp on my working permit (it was necessary for obtaining the visa later) and exchanged some dollars for dirhams. By the way, it turned out that Russian roubles couldn't be exchanged for dirhams there, at least officially - good thing I didn't have any.

![Airport](https://andlukyane.com/images/dubai_relocation_2022/20220506_080820.jpeg)

I used the Careem app to hail a taxi and quickly traveled to the hotel. It was astonishing to me that there were high-speed highways inside the city near the buildings. Unfortunately, I think I got heatstroke on the very first day - I felt pretty bad; it got better only by evening and after I took various pills.

I had two different UnionPay cards. They worked almost everywhere - shops, cafes, taxis - with several exceptions.

My company was handling the visa process, so I had only to go to a medical center to give biometry data.

For the first week, I didn't have any sim card at all - it was promised that we would get a corporate sim card on the first working day, but we got it only by the end of the first week. Also, I thought I would get a bank card within 1-2 weeks, but it took almost a month to get it.

In general, there was a lot of chaos with getting a bank card. On the first working day, we got a letter with the information that we could try to get an account in one of the three banks: Mashreq, Emirates NBD, and HSBC. I wrote to HSBC first but was ignored. Later I heard that they open accounts for Russians only if they have a full Emirates ID, and even then, not for everyone. So I wrote to Emirates NBD next and got an answer pretty fast. But it took longer to get the debit card itself because they refused to give it without a passport, and I left my passport to get the visa.

## Living in Dubai

Let me share an assortment of my experiences of living in Dubai.

What I had to learn is that Dubai is a city for cars, not pedestrians. Well, you can walk somewhere, there are some parks, but the infrastructure is built for cars - in some cases you just can't get to some places on foot, in other cases it may take too much time. So, for example, travel that takes 10 minutes by car could take a couple of hours on foot.

<div class="gallery" data-columns="2">
	<img src="{{site.url}}images/dubai_relocation_2022/20220507_183254.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220509_093349.jpeg">
</div>

The number of coronavirus cases in Moscow was low for a long time, so I got used to walking without masks. In Dubai, masks are widely spread, though not seriously enforced. For example, in a mall, you could be asked to put on a mask at the entrance, but later no one will control it - many people walk without masks. People working in the service industry always wear masks, though. And in our office building, there are notices with warnings about severe fines for walking without masks.

In Moscow, people usually don't ask about how you are doing, but if they do, they expect to hear an entire story, including your illnesses, your family's well-being, and your pets' habits. Here everyone asks, "How are you" and no one cares to hear the answer. It takes some time to get used to it.

I joined several telegram chats for Russians living in Dubai. The amount of advertising, spam, and fraud are astonishing. In the best case, people will advertise their services. In other cases, they will spam messages about crypto, escorts, illegal drugs, and other things. And many fraudsters are trying to squeeze money from desperate people.

There are a lot of Russians around. I often hear Russian speech in shops, cafeterias, and malls.

It was entirely unexpected for me when the waiters ask not for tips but for leaving reviews on TripAdvisor in different restaurants.

You can't always trust official information. For example, once, we (with some of my colleagues) decided to visit a falcon museum. When we arrived, we found it closed. It was closed for some time, but no one bothered to update the website information.

![Falcon](https://andlukyane.com/images/dubai_relocation_2022/20220625_143953.jpeg)

Big grocery shops in malls sell products from many countries, including Russia.

<div class="gallery" data-columns="1">
	<img src="{{site.url}}images/dubai_relocation_2022/20220516_110447.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220516_110456.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220516_110532.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220516_110747.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220516_110802.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220516_110831.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220717_134728.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220521_115040.jpeg">
</div>

Surprisingly, the sky is often greyish. I assume this is due to the dust.

There are a lot of big malls. During hot months people spend a lot of time there.

<div class="gallery" data-columns="1">
	<img src="{{site.url}}images/dubai_relocation_2022/20220511_153828.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220516_113752.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220521_121542.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220611_184129.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220611_192916.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220611_210000.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220618_155157.jpeg">
</div>

Dubai Mall has an awesome bookstore Kinokuniya. It has so much amazing stuff!

<div class="gallery" data-columns="1">
	<img src="{{site.url}}images/dubai_relocation_2022/20220710_114609.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220710_114610.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220710_114633.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220710_114647.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220710_114907.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220710_115018.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220710_115350.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220710_115517.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220710_115728.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220710_120410.jpeg">
</div>

I read a lot about stringent laws, but most of the information is outdated: you can live together without being married, you can find alcohol, you can walk in revealing clothes, etc.

A lot of the city is about tourism. There is a lot of entertainment for tourists. During the winter, there are many people from different countries, and the prices of apartments rise.

Some people may have some health problems here. For example, some may suffer from the air or water quality, some may suffer from the too bright sun, etc.

### visa

There are multiple types of visas in the UAE, but if you plan to work there, only several options are relevant:

* basic work visa. It is applicable for low-skilled workers - those who don't provide an attested degree and don't fulfill some of the conditions;
* a work visa for skilled workers. It is almost the same as the previous one but requires an attested degree and other things. The difference between them is pretty vague; I couldn't get any specific and detailed information. One good thing about having a working visa is that it allows you to be a "sponsor" for dependent persons - your spouse and children can get a "dependent" visa and live in Dubai. But the important thing is that a visa depends on a job. If you lose the job in Dubai (leaving the job or being fired) and don't get a new job soon, the visa will be annulled, and you'll have to leave the country;
* There is also a visa for a remote job, but I don't know much about it;
* one could try to get a golden visa, but the requirements for eligibility are pretty high;
* and getting citizenship is much more complicated, the requirements are severe;

### Where to live

There are multiple options for living in Dubai.

* First of all, there are hotels of course. If you can afford it, you can live in a hotel and don't bother with most of the domestic tasks and problems;
* Or, you could live in a hotel apartment or serviced apartment. This means that you live like in a hotel but have an entire apartment - with a kitchen, washing machine, and everything else;
* Of course, you could also rent a usual apartment. In fact, if you plan to take your spouse/children with you, this is the primary option, as one of the requirements is having a rent contract for at least 1 year. 
* There are other options, of course - renting a villa, sharing an apartment with other people, etc.;

The prices vary a lot depending on the district and the quality of the building/room. Also, it is imperative to investigate where you plan to live - some buildings may have mold, some may have insects, some may have technical problems, etc.

### Food

There are a lot of options for any taste. In many restaurants, the dishes are marked as vegetarian, dairy, etc. You can eat food from different countries and cuisines. You can eat well both for 20 and for 200 dirhams. And the portions are often huge. Also, due to summer's hot weather, you usually want to eat less.

<div class="gallery" data-columns="1">
	<img src="{{site.url}}images/dubai_relocation_2022/20220511_153828.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220512_130041.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220516_121636.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220518_135907.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220521_122017.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220522_143711.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220528_143116.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220604_135321.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220611_130031.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220618_153141.jpeg">
</div>

One bad thing is that a lot of restaurants have flies.

### Climate

The summer is scorching here. Pleasant weather lasts from October or November to April or May. Right now, the temperature is usually 40 degrees during the day, and it doesn't drop below 31 even at night. The humidity varies based on the location and the time of the day; it can be as low as 20% or as high as 70%.

<div class="gallery" data-columns="4">
	<img src="{{site.url}}images/dubai_relocation_2022/Screenshot_20220623-120131_Weather.jpg">
	<img src="{{site.url}}images/dubai_relocation_2022/Screenshot_20220702-164022_Weather.jpg">
	<img src="{{site.url}}images/dubai_relocation_2022/Screenshot_20220724-140121_Weather.jpg">
	<img src="{{site.url}}images/dubai_relocation_2022/Screenshot_20220824-175258_Weather.jpg">
</div>

To tell the truth, I expected it to be worse. I expected I wouldn't be able to go outside during the day, but I am able to do it. During the first weeks, I didn't feel well sometimes, and I think I got at least 1 heatstroke, but right now, it is okay.

If you want to go to the beach in summer, prepare to do it at 6-7 am. If you go later than this, you could get sunburned. For example, I swam in an open swimming pool for several days at 8 am and got a severe sunburn; it took nearly a week to get healed.

One more challenge is dust storms. They happen when the seasons change or in summer. Recently there was such an intense dust storm that there was an official warning to avoid leaving the buildings and even avoid driving.

<div class="gallery" data-columns="3">
	<img src="{{site.url}}images/dubai_relocation_2022/20220517_190957.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220524_174127.jpeg">
	<img src="{{site.url}}images/dubai_relocation_2022/20220814_094547.jpeg">
</div>

Generally, when you go out during the day, you need to wear clothes covering your body, an umbrella or sunscreen.

### Working in a different country

Maybe this is because I work in IT/DS, and it is universal, but many things are not that different. For example, people behave in similar ways, the amount of meetings is high, the infrastructure has similar problems, and the documentation's state is similar too.
One not-nice thing was when the hiring policy went from "we are actively hiring" to "we have a hiring freeze and stricter performance evaluation".
And vacation days don't transfer to the next year (there is a limit of 5 days).
But there are some interesting challenges and opportunities present here.

### Moving is difficult

Moving to a different country isn't an easy decision, especially when done in turbulent times. It was tough for me, too, especially because I have lived in the same place most of my life. Making such a radical change in my life was scary, but ultimately this is an interesting experience.

When you do this, you start thinking differently about many things. For example, you could have a lot of things at home but only take several bags with you - this makes you realize that many things aren't that important. Also, living in a different country may give you an opportunity to rethink your habits and behaviors and notice the differences and similarities between different countries.

And moving to a different country doesn't mean living in your homeland forever - it is possible to come back.

### Conclusions

When you live in a new country, there are always things that you are unaccustomed to. Some things are nice, and some things aren't. I like many things here: sunny weather, cleanliness, safety, variety of foods, shops, swimming in a pool several times per week, etc. Most people here don't care about the current situation with Russia and Ukraine. I don't know how long I will live here, for so far I like it.
