---
title: 'Lessons learned after 10 years in IT'
date: 2022-08-08 00:00:00
description: Some thoughts and ideas bases on my working experience in ERP-consulting and DS
featured_image: 'https://andlukyane.com/images/5yearscareeradvice/2022-08-02_07-55-25.jpg'
tags: blogpost career advice
---

## Lessons learned after 10 years in IT (DS and ERP-consulting)

![Main image](https://andlukyane.com/images/5yearscareeradvice/2022-08-02_07-55-25.jpg)

~5 years ago, I started my career as a Data Scientist, and before that, I worked as an ERP-system consultant for 4+ years. During this time, I have worked on many different projects and met a lot of interesting people. Now I want to share some of my experiences, describe some thoughts, and give bits of advice on various topics. I often get asked various questions about career, specific skills, and data science in general, so I hope this blog post will be helpful to many people.

But first, I want to point out that this is my own experience, with my own biases (including a survivorship bias) and preferences as well, as the things that worked out for me. I don't claim that all the ideas below are objective, quite the opposite. This blog post will be very subjective, and some may disagree. But nevertheless, I want to share my experience and hope that it will be of interest.

## Getting things done

If you were to remember only one idea from my blog post, then it should be the simple principle of "getting things done". Productivity hacks, networking, self-confidence, soft skills - nothing matters if you don't deliver the results. Sometimes, you can receive accolades in the short term thanks to careful manipulation and PR, but it won't work out in the long term. So the best thing is to have a mindset of producing the results and then using those results as building blocks for your career. I suggest reading a great book [Rise: 3 Practical Steps for Advancing Your Career, Standing Out as a Leader, and Liking Your Life](https://www.goodreads.com/book/show/12838919-rise) to learn about it in more detail.

### Find out your optimal daily amount of "intensive" work

![Daily working time](https://andlukyane.com/images/5yearscareeradvice/01-Image-scaled.jpeg)
https://www.7pace.com/blog/how-much-work-can-you-really-get-done
Various studies show that an average knowledge worker is productive for ~3-4 hours daily. Some people can focus longer; for others, the limit is lower. Of course, it is possible to push yourself and work longer. Still, it could lead to mistakes, burnout, and various other problems. It is better to spend the rest of the time networking with colleagues, mentoring, organizing knowledge, completing less focus-intensive tasks, and so on.

In fact, if you are too good at completing your current tasks, you may have problems in progressing your career: usually, each next career level requires new skills and perspectives. So if you don't spend enough time on improving your "next" skills, you won't be able to progress.

### On working overtime

![Overtime](https://andlukyane.com/images/5yearscareeradvice/overtime-negative-productivity.jpeg)

Sometimes we are asked to work overtime. Broadly speaking, I'd split all the cases into three general categories: working culture, unexpected and systematic emergencies.

Some industries or departments expect their workers to work overtime, for example, consulting, on-call support, etc. You should be aware of this beforehand and decide for yourself if you are okay with it or not.

Unexpected emergencies are irregular situations when you need to do something suddenly - fulfill an urgent request, fix some error, etc. The best way to deal with them is to sit down afterward, find out why, and ensure that this situation won't occur again. Unfortunately, preventing these situations isn't possible in some cases, and they evolve into systematic emergencies.

Systematic emergencies are situations when some process is broken: maybe managers regularly underestimate the time to complete the tasks, maybe the clients constantly change requirements within the same time limit of delivering the project, or maybe the infrastructure isn't well maintained and breaks down periodically. All these situations cause people to work overtime and usually don't have easy solutions. It is better to leave such situations unless you can ensure that they will get fixed.

Anyway, overtime is okay in the short term, but if you have to overwork for months, this will hurt your mental and physical health - I have seen enough examples of such situations.

### Not all ideas/projects are deliverable. So do everything possible to stop them.

![Stop impossible](https://andlukyane.com/images/5yearscareeradvice/0_BXO-Krt9rSiopn6k.png)

Sometimes, we are asked to do something that just isn't possible. It could be something almost impossible (like AGI), something not possible in a given timeframe (develop a complex product in 1 week), something not feasible with a current infrastructure (make a real-time system, while the data is updated only once per day), something not possible with a current budget (hire a professional DS TechLead for 10$/hour), something not possible for a required value of the metric (99% accuracy for a complex and domain-specific task) and so on.

The best approach, in this case, would be to present hard facts which show that the idea won't work out - statistics, examples of similar projects, and opinions of other people.

But often, the management won't listen to the experts: either they think they know better, or they have some other motivation - political, personal, etc. In this case, I suggest writing down your explanations and sending the e-mail so that you won't be blamed in the future when the project fails. And if you are blamed for this failure despite your warnings - it is better to find a new job with competent management.

And, while working on this project, try to derive some benefit from it - learning some skills, getting goodwill, making a talk about your experience at some conference.

### Find impactful things to do

Sometimes at a job, we have to do useful but routine things or projects with little impact. Try to make sure you spend at least some time on impactful projects. It is possible to do it even at a junior level.

### If you want something from other people, hound them (within reasonable boundaries)

It is worth remembering that everyone has their own problems, priorities, and concerns at a job. If you need something from another person that they aren't required to provide you, be prepared that you may be their lowest priority. People aren't malicious - they usually simply don't care. There are many stories on the internet that when a person returns from a vacation and sees several hundred incoming letters in their e-mail, they simply delete all the letters - because if something is important, they would be contacted again about it.

So, if you need something from another person, you can generally follow three approaches:

* go through the higher-level managers and persuade them that your request is important;
* offer to give something in exchange - help in solving their problem, offer something that your department can provide;
* hound them - frequently call, find their office and wait for them, write every day. Of course, this shouldn't break any reasonable boundaries, but you get the idea - make them think that it is easier to grant your request than ignore you;

### Time estimation is hard

![Time estimation](https://andlukyane.com/images/5yearscareeradvice/time-estimation-statistics-in-project-management.png)

Time estimation for any IT project is complex and even harder for DS projects due to the higher level of uncertainty.
Sure, if you have some routine tasks, recurring projects or projects similar to each other, it is possible to estimate the time necessary to complete them. But if you work on a new project, or need to get a new data source, or need to do something else that wasn't done in this company, it will be difficult to make correct estimates. Many things could go wrong, so be sure to increase the initial time estimates. A balanced approach could be to gather the information and research the topic, then produce some time estimations based on your analysis.

### Don't be afraid to ask questions

It is crucial to ask questions (without being too annoying, of course). Don't assume that you know everything - ask questions about what your bosses expect of you, the processes in the company, and so on.

## Random thoughts

### Data quality: garbage in garbage out

![Time estimation](https://andlukyane.com/images/5yearscareeradvice/Data-quality-and-standards-garbage-in-data-garbage-out-results-Fig-5-Question-1.png)

As Data Scientists, we know that we can't do ML without data; even if we have some data, many things could go wrong. So always analyze the data and tell the management that one can't get great results with garbage data if its quality is lacking.

* We can't guarantee good forecasting for several years if we only have data from several months;
* We can't guarantee a good performance of the models on the real-world data if we only have synthetic data;
* We can't guarantee a good performance of the models if the data was labeled without good validation;
* We can't guarantee a good performance of the models if we predict events based on external factors that we lack in our dataset;

And so on. Sometimes it is possible to mitigate the problems by adding pre- and post-processing, using hacks and more straightforward approaches. Still, they are usually costly to maintain and result in a worse performance.

On the other hand, don't shy away from working with dirty data. A week of cleaning or labeling the data could often save weeks and months for the project.

### Domain experience is significant, but it isn't always correct

Domain expertise is very important. It is usually a good idea to talk with experts and gather their opinions if you are working on a project. Their suggestions could provide you with new research areas, help create the features, and improve the validation of the model. And domain experience is even more helpful when working on a niche project.

On the other hand, don't forget that people are prone to making mistakes, so take their suggestions with a pinch of salt. Here are some examples of the problems with the domain expertise:

* In some cases, the domain experts may be plainly wrong. For example, it could be a case of "correlation doesn't imply causation" or maybe their ideas were correct some time ago, but things changed, and the experts didn't modify their knowledge according to the new information;
* It is possible that if you ask several experts, they will give you different or even contradicting opinions;
* Sometimes, experts could give incredible insights but fail to provide their reasons and line of reasoning. These people have a lot of experience, so their thoughts may skip some steps that would be necessary for an explanation in layman's terms. While such insights are helpful, it may be impossible to automate them;

## On interviews

### Interviews are broken from both sides

There are many styles of interviews in different companies: take-home assignments, online tests, live coding, system design, behavioral rounds, discussion of experience, and others. Most companies have multiple rounds for positions of middle and higher levels; there could be up to 5-10 different interviews.

People have different preferences - some refuse take-home assignments, some like them; some hate whiteboard interviews, some thrive at them - and often say that the interview process is broken. The problem is that it also seems to be broken from the other side of the fence.

I conducted some interviews myself and talked with other people who interviewed a lot of candidates; here are some of the reasons for the current complicated processes:

* some initial screening is necessary to weed out the obviously unlikely candidates. It decreases the time spent by more experienced people;
* now, if a person is interviewed by his future boss, it could be possible to combine technical and behavioral interviews into one. But in big tech, people are often hired into the company, and the department is decided later - in this case, it is better to have multiple points of view. While it is possible to have multiple people on the same interview, it could be more stressful for the interviewee, and interviewers could be influenced by each other;
* if the interview is for a senior or higher level, it is essential to evaluate the candidate based on multiple criteria - problem-solving, specialization, coding, etc. It is challenging to do this within one hour, so usually, several interviews are required;
* and soft skills usually require separate interviews, as they have their own sets of questions;
* of course, there is often a cargo cult present - oh, that big company has N rounds of interviews, we should do the same!
* and the cost of a "bad hire" is very high. Often it is better to miss some promising candidates than to hire the wrong person;

In general, I agree with such an approach, though I really dislike the situations when the interview questions have nothing to do with real job responsibilities.

I think we can treat interview preparation like an investment - we invest some time and effort and get a higher salary as an outcome.

And remember, the company doesn't aim to fail you; it simply wants to avoid bad candidates, so making some mistakes during the interviews is okay.

### Remember that you are evaluating the company too

It is important to remember that not only does the company decide whether you are suitable for them, but you yourself decide whether you want to work in this company. Read employees' feedback on the company on forums or special websites (like glassdoor). Watch your interviewers - do they look stressed or happy, are they nervous or relaxed? Ask questions during the interviews. Yes, you can and should do it. In fact, if you don't ask a single question, they would think that you don't care about the company and the job itself. I have an extensive list of questions and usually ask several of them; here are some examples:

* What does the working day look like? Are there daily/weekly stand-ups? Is it necessary to log time into timesheets? How are the tasks tracked?
* What is KPI? What are the ways to progress the career in the company?
* What is the infrastructure and hardware in the company, and what are the working conditions?
* How is the process of delivering the code organized? Are there code reviews? Is there an MLOps infrastructure?
* What are the common problems that happen during working in the company? How are emergencies handled?
* What is the level of freedom and control in the company?
* What do the interviewers like or dislike about working in the company? What were their favorite projects?

## On people relations and job politics

### There will be politics

You will be very disappointed if you think that working in IT means working only with computers and writing code. Workplace politics exist in any company, small or large. You may work without noticing any political intrigues, but it often means that you are being taken advantage of and simply unaware of it. The further you progress in your career, the more politics you will encounter.

### Many nuances of communication

![Communication](https://andlukyane.com/images/5yearscareeradvice/communication-in-data-science.gif)

Communication is one of the most critical soft skills. We communicate with many people at work, but not everyone understands that you must communicate differently with people and in different circumstances.

When you are doing presentations or delivering reports, you do it differently for different people: for a technical audience, you can pay attention to technical minutiae, but if you are talking to business people, you should drop most of the technical details and focus on what is important to them.

If you have a problem and need your boss to help you, then the best way would be by giving a well-formulated text and giving possible options.

And there are many other different styles of communication - learn about them and use them accordingly.

### On bad bosses

Sometimes we are lucky, and our bosses are great; often, they are okay, but there are many cases when they are just bad. Some are incompetent, some are manipulative, some are abusive, some don't help you at all, and so on. Know that this isn't your fault and it is better to leave the job than to suffer.

## On technical things

### Learn to be productive in small ways

![IDE](https://andlukyane.com/images/5yearscareeradvice/python-ides-1024x406.png)

I think spending some time optimizing your technical workflow is a good idea. Using hotkeys is a great example. I have seen a lot of cases when people spend a lot of time finding the correct buttons on the screen each time instead of hotkeys for common things. This will save you a lot of time and nerves.

Another example is typing fast enough that you can keep up with your thoughts.

Using suitable software is another case - there are a lot of different programs for similar tasks, so find the best one for you.

You could say that this isn't a bottleneck, that other things are more important. I don't argue that other things are more important, but using your tools efficiently is a mark of professionalism (I think). And it is better to eliminate technical inefficiencies so they won't distract you.

### Pareto principle of learning new things

You don't have to learn every single thing. But often, investing 1 day or 1 week on learning something new could broaden your experience, and help you immensely.

### Unused skills get rusty, but the foundation remains

If you are struggling with remembering something you learned years ago - know that this is entirely okay. It is completely normal to be unable to recall old memories and to lose some skills you developed long ago and didn't touch since then. We learn new skills and simply live our lives - we can't remember everything.

On the other hand, if you learned something very well, it will be easier to recall this information. So, don't slack when learning new things, but don't despair when you start forgetting them.

### Don't be ashamed to use whatever you like

![whatever you like](https://andlukyane.com/images/5yearscareeradvice/7wlt8u1cv5hd8ts4nvj5.jpeg)

There are a lot of holy wars in IT: use light or dark theme, use GUI or only command line, use this or that IDE, and so on. Use whatever you like and don't pay attention to those who think it is a "bad" choice.

## Working conditions

### Open spaces

![Open space](https://andlukyane.com/images/5yearscareeradvice/open-plan_office.jpeg)

Open spaces are widely used in big companies, and I dislike them. I'm not sure what a good alternative is (WFH is a great idea, but a separate topic), but I hope it will appear in the future.

There are a lot of downsides to open spaces:

* constant noise and other distractions. I have worked in open spaces without partitions and was constantly distracted by movements that were in my field of view;
* regular "wars" on the temperature of air conditioning;
* distractions from people wishing to chat or say hello. In most open spaces there are at least several people who shake hands with all other people in the office...;

And many other things.

I know that the main perceived benefit of open spaces is the possibility to communicate more efficiently, but it usually leads to unnecessary distractions for DS or IT.

### Remote work - the good and the bad

![Open space](https://andlukyane.com/images/5yearscareeradvice/0_F0XsiPWxjkEi6FJW.jpeg)

First of all, I want to say that I really like working remotely: I have worked from home for the last 2 years both in senior and leading roles and liked it: more flexible hours, less time spent on commute, comfortable working space, and many other nice things. But I have to admit that there are some downsides to remote work too. In short, it makes many things more complicated than the usual work from the office. The core problem is that most companies don't have an excellent remote culture and processes.

* If you are a junior, it is usually more challenging to learn new things. It is much easier to teach juniors in person than during the calls;
* If you are a lead, it is more difficult to ensure everyone is okay. Virtual 1-1 are good, but when you don't talk face to face, you miss a lot of clues, so it is more difficult to understand how people really feel;
* Working as a team is more difficult too. People often feel disconnected; they don't feel they work as a team - they may feel that they work individually. Forming and maintaining connections is more complex too;
* Company culture often isn't ready to support remote work. For example, if some people work at the office and some remotely, usually the former have more access to inside information;

In general, if the company genuinely makes remote work their priority and educates people about it, it should be great; otherwise, it has numerous problems.

### A Company isn't your family; it is a business

![Open space](https://andlukyane.com/images/5yearscareeradvice/Why-business-is-no-a-family-thumb2.png)

Always remember that the relationship between the company and the employer is a contract. If one of the parties doesn't fulfill the obligations, the contract will be terminated. In a normal situation, the employees do their job, and the company provides the salary and the benefits. But if a company has difficulty doing business, it will lay off some people. Likewise, if a person gets a good job offer, he will change jobs. This is normal too.

### Document everything you do at a job

Try to document everything you do at your job, officially and unofficially.

Documenting officially means writing e-mails, JIRA tickets, documentation, etc. It is especially important when there are conflicting points of view on some things, and it is crucial to have proof of your position and actions in the future when things go wrong.

Doing it unofficially is helpful because this way, you won't forget everything you did. And showing the list of completed tasks and projects is great when you ask for promotions or when you need to report the progress of working toward your individual goals.

## Different types of companies

### Tech- vs nontech-companies

Working in tech vs. non-tech companies usually is very different. One of the main differences is that non-tech companies often perceive IT (and DS) as a cost center, and tech companies realize that IT/DS generates money.
While you can do a lot of great things in non-tech companies, you'll usually struggle with a lack of resources and have to spend a lot of time making people accept your solutions. But, on the other hand, usually, there are many more low-hanging fruits.

### Startups (or small companies) vs. large companies

![Small and large](https://andlukyane.com/images/5yearscareeradvice/1_-XA_F7wl6Qm4BSFOJL3pYw.png)

The topic of startups vs. big tech is always a hot debate, and I want to share some thoughts from my side:

* It is a good idea to work a least in one startup and one corporation so that you can feel the differences for yourself and decide what you prefer;
* Usually, in startups, you work overtime, the priorities constantly change, and the infrastructure isn't well developed;
* There is a high probability that you won't work on "usual" machine learning tasks in a startup: either you'll work on basic analytics or apply state-of-the-art approaches to a cool product;
* In startups, you have a higher probability of doing something cool or significant; on the other hand, in large companies, your models will affect much more people. And large companies often have a bigger budget for R&D;
* In startups, you'll likely do a lot of things. This will give you a lot of experience, but it is possible that you'll get only shallow knowledge on a variety of topics and will have to learn everything by yourself;
* Usually, working in startups is more fun; everything will happen much faster;
* A lot of startups fail. Be ready for the possibility that the stock options will turn into worthless paper or the startup will close;
* There is much less bureaucracy in the startups, but it is quite possible that if things go wrong, there will be bitter arguments among the co-founders;

## General career

### Getting the first job is the most difficult thing

![First job](https://andlukyane.com/images/5yearscareeradvice/job-experience-catch22-525.jpeg)

It may sound strange, but getting the first job is one of the most challenging things in the career. Yes, working is difficult. Yes, further interviews are difficult. But getting the first job is highly challenging due to high competition. The amount of people who try to enter the field is very high. Therefore, after you start working, you will immediately gain a massive advantage over the other people - experience.

### T-shaped skills are great

![T-skills](https://andlukyane.com/images/5yearscareeradvice/1_3jJEfx1B2xX86Uc1_e0r1Q.png)

There are several ways of developing skills:
* You can be a generalist, a "Jack of all trades, master of none";
* You can be a specialist in a specific area;
* Or you could have t-shaped skills - basic knowledge of many areas and deep knowledge of some specific areas;

At the same time, data science has some specific specializations; for example, BigData usually requires knowing SQL, PySpark, and classical ML; NLP/CV/Audio requires knowing neural nets, etc. Usually, it is better to choose one of them than to pursue everything.

### When and whether to change your job

![Changing jobs](https://andlukyane.com/images/5yearscareeradvice/DSA-job-change-tenure-combined-by-exp-2018.jpeg)

Changing jobs every 1-2 years at the beginning of your career and then working longer at a specific place is a good idea. Most companies don't have a policy of regularly raising the salaries by 20-30%, and it is pretty easy to get this increase when moving to a different company. Later, when you get long-term stock options, staying longer may be a good idea.
This isn't only a matter of salary - when you switch jobs, you have an opportunity to work on different projects and different technological stacks. This is a valuable experience.
And if you were lucky enough to find a company with regular salary increases, an opportunity to work on different projects, and a good atmosphere - hold onto it.

### Present yourself well

First impressions matter, so learn to present yourself well.

* Write a good resume. Keep it concise, well-formatted, and without typos;
* If you write blog posts, make a nice-looking personal website. It really helps;
* Have a short self-pitch. We often need to introduce ourselves in several sentences - at meetups, during interviews, etc. Have a prepared talk about yourself. And it is better to have several versions with different amounts of details depending on the circumstances;

### Think long-term

After you reach a senior level (or even before), it is worth spending some time planning your career. First, ask yourself what you want: to be an IC or a manager? Do you want to work in a specific industry? Do you want to have a particular specialization? Of course, it doesn't mean you have to set your career path for your whole life, but it is better to have some plan than none.
After you have this long-term plan, try to follow it. You may get some excellent opportunities that don't align with your strategy - often, it is better to ignore them and focus on what is important to you.

### Learn to iterate fast

This applies to the career, specific projects, public activities, and many others.
Perfect is the enemy of good. It is better to make small iterations, gather feedback and improve than to spend a lot of time and effort and discover that you should have done something different.

### Develop your personal brand

![Changing jobs](https://andlukyane.com/images/5yearscareeradvice/1_aq1fvYwEsEsjA_Ko3gRhBw.png)

Many people don't like the phrase "personal brand" and think this is an unnecessary PR. And many people believe that they shouldn't spend time on it.

In my opinion, this is wrong. First, realizing that your "brand" is being developed with or without your input (even if you don't recognize it) is essential. In layman's terms, your brand is what people think when they hear your name.

* Do you work in startups or large corporations?
* Do you make chatbots? Object detection? Recommender systems?
* Do you give talks? Contribute to open source? Write blog posts?

Etc. Your actions create your brand. The only thing you can do is to work on your brand consciously or not.
The brand can not only help you become well-known, but it will also help you in your efforts and provide more opportunities in what you do.

And, of course, your brand will evolve over time; it is normal.

### Maintain a good reputation and networking

Earning a good reputation is difficult, demanding, and slow. On the other hand, losing a reputation can be very fast and challenging to recover.
Start working on it early on, and it will pay off.

### Different types of data scientists

![Different DS](https://andlukyane.com/images/5yearscareeradvice/1_bgvRyySdqyxC5UA5ziQM9Q.jpeg)

There are many types of data scientists.
Some pay more attention to the domain/business side, some do more ML, some prefer deploying the model and writing better code, and there are more types.
One person can hardly be able to do everything at the same time, so don't try to learn everything and keep all your skills up-to-date. Select what is important to you and work on it

### Power of communities, big and small

I think it is imperative and valuable to take part in DS communities. There are many different communities:

* Some are based on specific products;
* Some are created by a small group of people;
* Some are large communities encompassing a wide range of topics;

It is a good idea to join such communities and participate in them. You can get a lot of helpful information, find friends, get new opportunities, and so on. Large communities may be able to give more details, and smaller communities may provide better networking.

### On growing between levels

![Career levels](https://andlukyane.com/images/5yearscareeradvice/2022-07-27_16-49-39.jpg)

It is crucial to understand that different levels have different required skills, and there are different expectations of you.
The higher you go, the higher freedom of choice you have, but at the same time, you have more responsibilities and the more you have to think about the impact of your actions.

Of course, there are many different ways to grow:
* Tech lead or staff/principal is more about technical skills, developing the architecture, and having good technical expertise;
* Product/project management is about ensuring that products/projects are successfully delivered;
* Team leading is more about managing people;

In general, if you want a leading position, there are two ways to get it: getting a promotion within your company and switching the job.

If your company and department are growing, it can become a lead inside it. On the other hand, if the development stagnates, you can only hope that one of the other leads leaves the company.
One of the best ways to work towards the promotion is to start taking on more responsibilities and demonstrating your leadership skills. And, of course, you need to discuss this with your boss beforehand.

In the case of switching companies, it could be challenging to prove you have the required skills for a lead if you didn't work as a lead before. On the other hand, many actively growing companies are eager to hire a senior DS as a new team lead.

Also, it is worth remembering that you don't have to become a lead. Many companies have a career ladder for individual contributors; not everyone wants or has an aptitude for leading positions.

### On developing skills in your free time

Everyone wants to spend their free time doing pleasant things, but people in IT/DS usually have to spend some time maintaining and improving their skills. Our field is snowballing, and it is vital to keep up with the new developments, especially deep learning.

Make sure to improve your skills and balance them with your life. Find some things that you enjoy doing and work on them. Watch talks - this way, you'll be exposed to the experience of many different people in different industries.

### Learn to google well

It surprises me, but many don't know how to search for information. I have encountered many situations when people say, "I tried to search on the internet but found no answers to my questions", but when I google, I usually get relevant results after the first attempt.
Learn how to formulate concise search queries; it will save you a lot of time.

## Kaggle

![Kaggle](https://andlukyane.com/images/5yearscareeradvice/thumbnail.png)

Kaggle is a very controversial topic in Data Science. Some people are passionate about it, and some don't care at all. Some say that it is fantastic, and some are sure it is a useless toy.

I think the truth is somewhere between: those who never took part in Kaggle seriously usually underestimate it, and those who spend all the time on Kaggle sometimes overestimate it.

Let me provide some arguments both for and against Kaggle.

I'll start with the good things:
* This isn't fun, but kagglers have a lot of experience working in stressful situations, in the cases of tight deadlines and changing environments. This can use helpful for startups;
* A vast experience with the tasks from different industries and domains;
* A developed intuition of what approaches to try, what models to train, what hyperparameters to use, and what features to create;
* An experience in solving non-trivial tasks, optimizing metrics, and finding out what went wrong;
* Qn experience of a fast iterative approach, reproducibility, and vigorous evaluation of the models;
* I want to repeat the point about vigorous evaluation. When you participate in Kaggle competitions, you get fast feedback during it and final feedback in the form of a private leaderboard. This teaches you to validate your models correctly. In the industry, the feedback cycle could be much slower (of course, if you have a developed and automated infrastructure, it will work well), and you could make some mistakes. I saw a lot of cases of wrong validation approaches in the industry, which led to overestimation of metrics values;
* It helps your personal brand;
* Kaggle isn't only about taking part in the competitions: you can read the solutions to the old competitions, you can read/write code in public notebooks, you can get some data, you could study courses, etc.;

But, as I said, there are many problems with Kaggle:
* Several years ago, the industry was more Wild West than now (though it still is). Kaggle was a great indicator that you know how to make things done. Now there are better indicators, and companies understand better what they need;
* Several years ago, we thought that more companies would consider Kaggle while looking for candidates and conducting interviews. But their number didn't increase much;
* ML is only a part of a project (even if it be the critical part);
* Too much gamification. For example, there were dozens of threads where people posted memes to get upvotes (for discussion ranking). Some people simply repost content from the internet;
* Often, the code written on Kaggle isn't good;
* Yes, training dozens/hundreds of models often (though, now always) is necessary to win;
* Some companies hire people to compete on Kaggle. They have a lot of experience, time, and hardware - so tough to compete with them;
* Often, you need to overfit the test data;
* Often, you need to use questionable tricks in real-world: multiplying the predictions to match the test data, abusing the leaks, reverse engineering;
* There are competitions with leaks;
* The more experience you have, the less important Kaggle is for your career. Being a competition master is fantastic for a junior, but if you have 5+ years of experience, it isn't much;

For me, Kaggle was very useful: I got a lot of skills, met many amazing people, and developed my personal brand, which helped me in my career. But I understand that it isn't valuable for "common" ds jobs: if ML is <5% of your job, it doesn't really make sense to invest time and efforts in Kaggle.

## On mental things

An assortment of various thoughts about mental challenges and what you can do about them.

* Nowadays, it is too easy to be distracted by something, especially by our smartphones or by working on multiple tasks simultaneously. Recent studies show that humans are much more efficient when they work on a single task without switching between several. Train yourself to work on a task without losing your concentration. [Deep Work: Rules for Focused Success in a Distracted World](https://www.goodreads.com/book/show/25744928-deep-work) is an excellent book about it. This way, you'll be much more productive. And you'll have more time for fun activities :)
* See the forest before the trees. We often lose sight of the bigger situation in favor of solving minor problems. Be sure to spend some time thinking about what you do, whether it matters, and what can be done differently;
* Set definite boundaries of the things that are acceptable to you. For example, you could be against working overtime, you could be against working in specific industries or companies, and you could be against breaking some moral principles. And if someone makes you "flexible" on your boundaries, make sure that there is enough compensation for it and that it is limited - otherwise, they will try to force you to break your boundaries again and again;
* Know when to say NO. There will be many cases when some people want something from you. Know when to refuse their requests. It isn't possible to help everyone, especially when it requires sacrifices on your part;
* At the same time, be open to new opportunities. You'll often get a chance to try something new. If it doesn't cost you much, it may be a good idea to try it;
* Get used to learning new things and to being wrong. The process of studying new things (be it at work or in your personal activities) can't be done without making mistakes and learning from them. Don't take the mistakes personally - everyone makes mistakes. Just learn from them and try to make sure you won't repeat them;
* An impostor syndrome is real, but you can overcome it. If you think that you are a fraud, that you won't be able to complete your project, and that you are much worse than others, know that almost everyone has the same thoughts and fears. I can't give a definite solution to this problem, but know that you aren't the only one doubting yourself. It will be better after some time;
* Don't compare your worst with others' best. Don't compare your boring projects to cool talks. Don't compare your losses in competitions to the winner's presentations. Compare yourself to the other people of your level. Remember that for each story of success, there are multiple stories of failures. And remember that all cool people around you had their own share of problems, failures, and difficulties;
* At the same time, you should be "afraid" at the beginning of a new job. It is normal to feel our depth when you start working at a new place. Work on solving the problems step by step;
* Keep up your work-study-life balance. Yes, people often speak about work-life balance, but I think that it is essential to weigh in time spent on studying too;
* Burnouts are real; many people experience them. Make sure to notice the early symptoms of burning out and try to deal with them fast. Some examples of the early signs could be: procrastinating instead of working on the current project, wishing to return home as soon as you arrive at job, not being interested in the project which excited you previously. I have experienced multiple burnouts, and it took a lot of time to deal with the consequences. And remember that burnouts can happen not only due to overworking but due to underworking too;
* Discipline and motivation. For me, this is a controversial topic. On the one hand, motivation is fundamental as it helps you to start working on things and getting them done. On the other hand, [motivation](https://drawabox.com/article/motivation) is fickle. Discipline will help you to preserve even if motivation disappears;
* Think of the time spent on self-study as an investment. Yes, maybe you don't want to spend several hours every week on self-study, but think of it differently: spending X amount of time may increase your salary by N%. This could be a good trade-off;
* Think about a time when you were at the top of your game, excited, got great results, and people were cheering. Focus on what gives you energy and what was involved;
* Figure out your natural strengths. Spend less time on fixing weaknesses. Don't over-improve your weaknesses. If you're not good at something, work on it until it no longer prevents your progress, but the bulk of your time is better spent maximizing your strengths;
* And, I repeat once more that it is okay to do nothing if you are content;

