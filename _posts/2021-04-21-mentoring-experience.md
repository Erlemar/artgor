---
title: 'My mentoring experience in Data Science: lessons taught and learned'
date: 2021-04-21 00:00:00
description: My experience of formal and informal mentoring for people at my job and outside of it.
featured_image: 'https://andlukyane.com/images/mentoring_experience/jon-tyson--xcojCHOSTU-unsplash.jpg'
tags: blogpost datascience career mentoring
---

![Main image]({{site.url}}images/mentoring_experience/jon-tyson--xcojCHOSTU-unsplash.jpg)

<sub><sup>Photo by <a href="https://unsplash.com/@jontyson?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Jon Tyson</a> on <a href="https://unsplash.com/s/photos/mentor?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a></sup>

What is mentoring? How can I find a mentor? Questions like these are often asked on forums, on social media, and in communities. Mentorship is supposed to be an exciting situation when an experienced person shares insights and guides a mentee on a path to success pro bono. Some people suggest finding a mentor at work; others say that you can reach out to a potential mentor anywhere.

But how do things proceed from the mentor's point of view? Maybe one day, a person just decides that it is time to mentor and starts helping people? In my case, it started gradually.

For a long time, I thought that I was simply helping people reaching out to me. But then, at one job, I started officially mentoring people. After the end of the project, I looked back and realized that I have already mentored a lot of people. Previously I have thought that mentorship is a significant and formal commitment, but now I know that isn't always true, and mentorship can be completely informal and fun.

In 2020 I got an award for mentorship from ods.ai, and now I think it is time to talk about mentoring.

In this blog post, I want to share my experience of mentoring, my thoughts about it, the benefits for the mentor, and the potential difficulties.

## What is mentoring

There are many definitions of mentoring focusing on different aspects. [Wikipedia](https://en.wikipedia.org/wiki/Mentorship) provides the following one:

> Mentoring is a process for the informal transmission of knowledge, social capital, and the psychosocial support perceived by the recipient as relevant to work, career, or professional development; mentoring entails informal communication, usually face-to-face and during a sustained period of time, between a person who is perceived to have greater relevant knowledge, wisdom, or experience (the mentor) and a person who is perceived to have less (the protégé).

I mostly agree with it and want to highlight the most important points:

* The mentor is more experienced in the area in which the mentee is interested. It is self-explanatory, but it also implies that the mentee could be more experienced in other areas. For example, the mentee is switching career or studying a new area of interest;
* The mentee has a specific goal (for example, getting a better job). If the mentee doesn't know what they want, the mentor won't be able to help;
* Usually, mentoring is conducted between two people, not in groups. The only exception is when the mentor helps several mentees simultaneously on similar topics - for example, mentoring newer colleagues at the job. But nevertheless, these group sessions should be less frequent than more personal mentoring;
* Mentoring is more personal and less formalized than other forms of knowledge sharing. For example, coaching is limited to business topics, and in the case of teaching, students usually have less freedom of choice, and teachers are less prone to share personal thoughts;
* The start of mentoring often isn't formal. In fact, there were only several people who wrote to me "become my mentor", and I usually refused (I talk about my reasons at the end of this blogpost);
* Mentorship communications can happen face-to-face, but I prefer messages or calls.
* Mentoring can be done infrequently, but it must happen at least several times. One-time advice isn't a full mentorship, though it still can be beneficial;

The mentorship itself has many forms: mentor may give vague advice or help with technical problems, provide a new perspective, motivates, supports and inspires, and helps in many other ways. The mentorship style depends on the mentor's preferences, the mentee's goals, and the setting (for example, whether it is at a job or not).

## My mentoring experience

![Boromir meme]({{site.url}}images/mentoring_experience/download.jpg)

At first, I want to share a brief overview of my experience at the time of starting my DS career: after graduating from the university, I have worked for ~4 years as an analyst/consultant in the ERP-system implementation sphere. During that time, I got Green Belt in Lean Six Sigma.

I have learned a lot thanks to working in consulting, notably talking with clients, discussing their needs and formalizing them, explaining tasks to programmers, testing the results, organizing projects, and many other skills.

Then I left this career, and after 8 months of self-study, I started working as a junior DS is a bank. While working there, I have completed two courses and did a pet project. This pet project involved gathering data, training the model, deploying it, and making a web interface to interact with it. Also, I was asking and answering questions in the ods.ai community.

As a result, even though I was a junior DS, I successfully switched my career to data science, had a variety of skills and knowledge, as well as an experience of a completed project (even though it was outside the job). This meant I already could help people, and, in fact, I had started mentoring during my first job as a DS.

### Informal mentoring

I'm going to separate formal (at a job) and informal (outside the job) mentoring because they are quite different: they have different levels of commitment, different motivation of participants, different expectations, etc. Describing all my mentoring experience in detail would be too long, so I want to do it only for my formal experience of mentoring. For informal mentoring, I'll cover higher-level information.

#### The first mentoring experience

![Drake meme]({{site.url}}images/mentoring_experience/56jn9t.jpg)

As I have already mentioned, I have started mentoring while working at my first DS job. A member of the ods.ai community has reached out to me with questions about switching careers, and I have shared my experience and gave suggestions on what to do.

Then we continued our communications, and I helped him with technical and theoretical problems, gave suggestions on various projects. One of them was about making a website and deploying a model, which was quite similar to my pet project mentioned above.

I have learned a lot of helpful information myself while helping to find answers to these questions.

Our communications went for 6-12 months.

#### Kaggle

As you may know, I'm Kaggle Competition Master and Notebook Grandmaster (I even held the top-1 rank for ~1 year).

When I was active on Kaggle (currently, I'm less active), I often answered questions on Kaggle forums. And many people wrote to me with questions - on social media, using e-mail, and through other mediums. In some cases, our communications have continued for a long time. The most notable examples are two people who became Kaggle Notebook Grandmasters; here is one of them:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">I&#39;m so excited to become a <a href="https://twitter.com/kaggle?ref_src=twsrc%5Etfw">@kaggle</a> Kernels Grandmaster after two years of work! I want to thank everyone who made this possible, including my family and friends, my inspiration <a href="https://twitter.com/mikb0b?ref_src=twsrc%5Etfw">@mikb0b</a>, and my unofficial mentor <a href="https://twitter.com/AndLukyane?ref_src=twsrc%5Etfw">@AndLukyane</a>! Now it&#39;s time to work on research and competitions! <a href="https://t.co/QHTs0hPQwy">pic.twitter.com/QHTs0hPQwy</a></p>&mdash; Tarun Paparaju (@SrirangaTarun) <a href="https://twitter.com/SrirangaTarun/status/1244570343680790528?ref_src=twsrc%5Etfw">March 30, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

I have shared my experience of writing and promoting ~~kernels~~ notebooks, provided feedback on their work, gave ideas and suggestions - and it worked pretty well!

#### Short-term and long-term mentoring

I was active not only on Kaggle - I answered questions in several communities, gave talks, and took part in many other activities. As a result, a lot of people reached out to me.

Some of them asked questions on a specific topic or wanted help in solving some technical problem (for example, asking about approaches to churn prediction or solving a specific medical CV task). Others wanted advice about career or general guidance. Usually, it was by text, sometimes using voice.

In most cases, one session was enough, so I'm reluctant to call this "mentoring", but I could help these people anyway, which is fine.

But in some cases, our communications continued for a long time. Sometimes we messaged each other frequently, sometimes we could go for months without any contact.

### Formal mentoring

I was a formal mentor at one of my jobs. Mentoring was a part of my responsibilities, but sometimes it is challenging to separate mentoring from other job activities when you are a tech lead. Thus, some of the following points will be purely about mentoring; others will be about managing the team or other job stuff.

The project was about developing a product using deep learning models. The work on it started long before I joined the company, but some company changes led me to become a tech lead of an NLP part of this project.

At the beginning of my tech leading, there was only one person in my team, but I hired 3 more quite soon. Therefore, I had to mentor both experienced and inexperienced colleagues. There was an additional problem - this coincided with the start of the covid-19 pandemic and lockdowns, so everything was remote, which posed different challenges because I wasn't used to it.

#### Onboarding

![Yoda meme]({{site.url}}images/mentoring_experience/56johh.jpg)

Onboarding is a particular type of mentoring - it helps a new person become a part of the project, the team, the company.

It is mostly about everyday things - helping to know more about the teammates, describing the processes, and informal rules.

In the ideal world, it isn't necessary to teach any technical things specific to the project - they are described in the documentation and internal wiki, but this is rare in reality. Best case - there are helpful documents, but they are a little outdated.

In my case, I thought that existing documentation wasn't enough, so I had 2-3 calls for several hours in total with each of the new people. I have told about the project in high- and low-level details, showed the code, demonstrated how it works, explained our development practices, etc. And then, we updated the documentation and tried to maintain it.

#### Initial teaching

I had people with different experiences on the team, and it was expected that some of them had more to learn than others.

From the very beginning, I have stated that any question is welcome. And if someone spends more than one hour on a purely technical problem - it is better to ask for help than spend too much time on it. So we often messaged or called each other throughout the week.

Sometimes I organized live coding sessions where I demonstrated how I would tackle a particular problem.

By the way, I think one of the most valuable perks of mentoring is seeing the mentor's thought process. Sending links to solutions/articles/courses is fine; giving hints is also good. But when you show your thought process - this is really valuable to mentees.

#### Developing the ownership mentality

Thanks to the project's structure, it was possible to give separate parts of the functionality to the different people. The code for it was mostly written, but there were plenty of opportunities to try new things. I set some necessary tasks, of course, but also encouraged people to try their ideas. Some of the ideas failed, some of them exceeded my expectations.

Of course, it wasn't always easy. For example, sometimes people went for advanced approaches without trying more straightforward methods, which hurt the project's progress. The solution was in setting specific minimal things, which should be tried, and then anyone could do anything beyond it.

#### One-on-one

![One more meme]({{site.url}}images/mentoring_experience/56jp57.jpg)

I won't say this was a failure on my side, but I could have done it better in hindsight.

How I did it: at the beginning of each month, I wrote to people in my team (separately, of course) and asked for feedback, for questions, for worries, and any other things which they wanted to tell me. And then, we discussed them, and I tried to solve the problems if they appeared.

How should I have done it: I think it would be better to have video (or at least audio) calls - seeing or hearing another person gives much more information and clues than text messages. Also, I should have given more feedback from my side (I gave it, but not always).

#### Weekly calls

After the team was formed and initial questions were solved, I have realized that I needed two things:

* Some kind of regular informal meetings. Agile/scrum meetings don't count - they are primarily about the job, and there are too many other people. But it is vital to have an opportunity to talk to each other, especially considering we were fully remote and had less contact with each other than in the office.
* Some way of teaching the same things to the whole team and not to each person separately.

My solution was to have weekly calls (usually audio, but sometimes also video) which lasted for 1-2 hours and consisted of two parts:

* At first, someone (often me, but after some time also other people) gave a talk/seminar on some topic. Usually, it was something practical: it could be sharing the progress on the functionality, some general issue, or a solution for a specific technical problem
* An informal talk about the job, the life and anything else

I think it worked out amazingly well.

* We have started with introductory topics and gradually moved to more advanced concepts; some of the issues were: writing the code for training a neural net in Pytorch from scratch, NER modeling, using Snorkel, Docker, ONNX;
* I have encouraged people in my team to give at least one talk on these weekly calls. It helped them to develop or improve their public speaking skills;
* We were able to know each other much better and maintain communications even after the end of the project;

#### General principles of leading the team

![A nice image]({{site.url}}images/mentoring_experience/isaac-smith-AT77Q0Njnt0-unsplash.jpg)

<sub><sup>Photo by <a href="https://unsplash.com/@isaacmsmith?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Isaac Smith</a> on <a href="https://unsplash.com/s/photos/progress?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a></sup>

It is difficult to describe everything which I did, but I want to name the main ideas and principles:

* tried to cultivate a good environment/culture in the team, aimed at helping each other and improving skills;
* as I have already said higher, I encouraged people to ask questions, especially if they were stuck. And I tried to help to solve both theoretical and technical questions;
* offered opportunities to work on tasks not only alone but in pairs;
* I taught practices of writing good code: setting pre-commit hooks, making style checks using ci/cd, code review, and other things;
* helped to improve soft skills, gave general advice of moving forward;
* pushed to improve our technical solutions - be it improving small things, automating whatever possible, using better and modern approaches;
* tried to give a lot of positive feedback, because previously I experienced a lot of situations when the things, which I accomplished, weren't praised/acknowledged;
* One additional point - I was lucky that one of the colleagues also wanted to mentor, and he took part in these responsibilities from me and helped less experienced teammates;

## Mentor's point of view

Now that I have described mentoring in many details let's talk about higher-level aspects of mentoring.

### Benefits of mentoring for the mentors

![So you are meme]({{site.url}}images/mentoring_experience/56jq9u.jpg)

Now that I have described my mentoring experience, you could ask - why do mentors even do it? What do I get out of it in exchange for my time and efforts? Well, some of mentoring is done purely like volunteering, but there are also a lot of benefits, which mentors get. And this is reasonable - because mentors spend a lot of time and effort on their mentees. There were periods when I spent up to ~5 hours per week on mentoring. I'll start with more idealistic justifications of mentoring and gradually move to the more practical ones.

* Mentoring is one of the ways of paying back to the community. I got a lot of help in ods.ai, kaggle forums, and other places and wanted to return the favor;
* I didn't get enough mentoring at my previous jobs, so I wanted to things "right" when I had such an opportunity at my job
* Often, it is effortless, especially in cases of short-term or infrequent mentoring. You can give a direct answer or steer in a different direction. This costs several minutes of the mentor's time but can save hours for the mentee
* Also, it is satisfying - seeing how your mentees improve and achieve success
* I think that one of the most common motivations is simply a desire to share the experience. People write blog posts, give talks, make videos to share their knowledge to help people avoid mistakes. Well, and, of course, to tell people how awesome they are, but this is less applicable to mentoring
* It is also commonly said that the best way to understand something is to teach it to someone else. And mentoring gives a perfect opportunity to do it. It helps to structure and systematize knowledge, improves and polishes fundamental skills. And if multiple people ask similar things, you could share this expertise later with public
* Mentoring, like any discussion, allows looking at some problems from a different point of view. Often a fresh opinion could lead to new and better solutions, which you could use in the future
* Mentoring also helped me to learn a lot of new things. Sometimes people asked me questions that were just a little outside of my expertise. But thanks to my knowledge and experience, I could learn these things fast and help solve the problem. Also, sometimes I have ideas but don't have time or opportunity to try them; I can offer these ideas to the mentee and see the results
* Mentoring also improves communication and other soft skills
* Mentoring increases your social network. You never know what your mentees will achieve. Chances are they will help you in the future
* Finally, you can write a blog post about your experience :wink:

### When the mentors don't mentor

It is essential to repeat once again that different mentors have different styles, skills, and preferences. And this means that sometimes the mentors can decline the request of mentoring because they can't or don't want to fulfill the mentee's expectations.

I have already mentioned that I have refused to become a mentor in the past. I want to share some of my reasons for these decisions.

* I was afraid of commitment. Yes, it was that simple. I was scared to become a full mentor and fail my future mentees, so I refused.
* I don't want to teach elementary stuff (doing it at a job is an exception). If someone asks to explain how logistic regression works, I will give links to good articles, and that's it. One of the reasons is that there are already enough high-quality resources on these topics. On the other hand, chances are that I didn't brush these topics for years, so it could take a lot of time to remember them.
* We don't always have enough free time. There are many DS activities, hobbies, and life itself - sometimes you don't have enough time to teach other people.
* Some people want to be spoon-fed. They don't want to receive hints or suggestions - they expect direct answers and ready solutions. Some people expected me to solve their problems and send them the complete working code when they did nothing from their side.
* They want mentoring in the topics I'm not interested in.

### Do you need mentoring?

![Distracted meme]({{site.url}}images/mentoring_experience/56juau.jpg)

Now, this is a good question.

I'd say that the main advantage of mentoring is getting the results much faster than without it.

Imagine a situation when you need to complete a task, which you aren't entirely familiar. You can ask for advice on forums, on social media, in communities. You can search for papers, blog posts, videos, and courses. After this, you'll get a pile of materials, a lot of suggestions (some of which can be conflicting), and many things to try. Of course, you can work through everything by yourself or use the recommendations from the most recent and popular sources. But the mentor could help you avoid the typical mistakes and point out what works on practice, which will save you a lot of time. And, what is more important, the mentor could share some ideas, based on experience, which you didn't even think about.

On the other hand, if you don't have a mentor right now, it could take much longer to find them, so it may be faster to solve the problem by yourself.

### Do I want to have a mentor?

It may be ironic, but I had little opportunity to have mentors.

I have learned many things on forums and in communities, at conferences, and at my job. There were people, whom I could call mentors, but usually, it was short-time. There were (and still are) people with whom we talk and share the experience. But I don't think I have ever asked anyone to be my mentor.

It was partly due to the diffidence and self-doubt, partially because I was used to self-study, partly because I was interested in a lot of things, did a lot of activities, and thus didn't know what I would do in half a year.

Would I like to have a mentor? Of course. But at first, I need to find an answer to the question, "What would I like to be taught, and what do I want to achieve".

### How to get a mentor

![Cat meme]({{site.url}}images/mentoring_experience/56juj4.jpg)

Let's return to one of the first questions raised in this blog post. Finding a mentor is challenging; often, people don't know what to expect from mentorship, don't know what they can offer in exchange, don't have goals. I'll share several thoughts about it:

* The most important thing - the mentee and the mentor should be compatible. They may have different mindsets, different ways of expressing things, other preferred methods of teaching/learning - sometimes mentoring doesn't work out;
* You need to have a specific request, which isn't generic. If you write "Help me become DS", chances are you'll receive a generic answer back or a link to a blogpost;
* Don't expect the mentor to write a complete solution for a technical problem if you didn't try to do anything by yourself;
* Some mentors require potential mentees to pass specific tests because they don't want to teach everything from scratch;
* There are also cases when mentors post on social media that they are accepting mentees - this could be an ideal to find a mentor;
* Learn more about the mentor and ask relevant things. Don't ask for mentoring in audio processing if the mentor has never worked with audio;
* If you want to have a higher chance of succeeding, try to reference something from the mentor's activities at the initial contact. For example, say that you were interested in one of their talks and ask for more details;
* This depends on the mentor's character, but some mentors are ready to accept a request for mentoring; some prefer to simply answer questions. Try different approaches with different people;
* If it is possible - offer something in exchange for the mentoring. For example, if you want a mentor to help you with the ML competition, offer them a place in the team in exchange for the mentoring (though you didn't make any submission to the competition, this approach could fail). Offer monetary compensation only if the mentor is known for providing paid mentorships; otherwise, it could lead to rejection;
* If your company provides mentorship programs - this could be the best choice;

## Conclusions

So, this was my experience of mentoring and my thoughts about it. I hope my blog post made it more transparent how mentoring looks like from the mentor's side and its difficulties and advantages. Not everyone needs a mentor, especially if it is possible to get support from online communities, but getting a good mentor may give a massive boost to your skills and career.
