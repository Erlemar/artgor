---
title: 'Paper Review: NÜWA Visual Synthesis Pre-training for Neural visUal World creAtion'
date: 2021-11-25 00:00:00
description: My review of the paper NÜWA Visual Synthesis Pre-training for Neural visUal World creAtion
featured_image: 'https://andlukyane.com/images/paper_reviews/nuwa/2021-11-25_18-19-08.jpg'
tags: paperreview deeplearning cv transformer pretraining
---

## NÜWA: Visual Synthesis Pre-training for Neural visUal World creAtion

[Paper link](https://arxiv.org/abs/2111.12417)

[Code link](https://github.com/microsoft/NUWA) (no code yet)

![Main image](https://andlukyane.com/images/paper_reviews/nuwa/2021-11-25_18-19-08.jpg)

In this paper, the researchers from Microsoft Research Asia and Peking University share a unified multimodal pre-trained model called NÜWA that can generate new or manipulate existing visual data for various visual synthesis tasks. Furthermore, they have designed a 3D transformer encoder-decoder framework with a 3D Nearby Attention (3DNA) mechanism to consider the nature of the visual data and reduce the computational complexity.

NÜWA not only achieves state-of-the-art results on text-to-image generation, text-to-video generation, video prediction, and several other tasks but also demonstrates good results on zero-shot text-guided image and video manipulation tasks.

-------

![Tasks](https://andlukyane.com/images/paper_reviews/nuwa/2021-11-25_15-23-54.jpg)

## The method

![Structure](https://andlukyane.com/images/paper_reviews/nuwa/2021-11-25_15-33-09.jpg)

### 3D Data Representation

All types of the data (texts, images, videos or their sketches) are viewed as tokens and are presented as 3D objects HxWxSxD, where h and w - the number of tokens in height and width, s - the number of tokens in the temporal axis, d - the dimension of each token.

* texts: 1x1xsxd (1 because there is no spatial dimension). Lower-cased BPE for tokenization;
* images: hxwx1xd (s == 1 as there is no spatial dimension). They train VQ-GAN for tokenization;
* videos: hxwxsxd (s is the number of frames). VQ-GAN on each frame for tokenization;
* sketches of images: hxwx1xd. They take a sketch mask HxW, transform it into HxWxC, where C is the number of segmentation classes. Then a separate VQ-GAN for tokenization;
* sketches of videos are hxwxsxd and are tokenizes similar to the previous points;

### 3D Nearby Self-Attention

![Attention1](https://andlukyane.com/images/paper_reviews/nuwa/2021-11-25_17-16-31.jpg)
![Attention2](https://andlukyane.com/images/paper_reviews/nuwa/2021-11-25_17-16-41.jpg)

![Attention3](https://andlukyane.com/images/paper_reviews/nuwa/2021-11-25_17-49-50.jpg)

![Attention3](https://andlukyane.com/images/paper_reviews/nuwa/2021-11-25_17-50-26.jpg)

### 3D Encoder-Decoder

To generate a target Y under condition C, the positional encodings for both of them are updated by three learnable vocabularies; then the condition C is passed through a stack of L 3DNA layers. The decoder is a stack of L 3DNA layers too. It calculates self-attention of generated results and cross-attention between the results and the conditions. The initial token V<sub>0,0,0</sub> is a special `<bos>` learnable token.

### Training Objective

The models are trained on three tasks: Text-to-Image (T2I), Video Prediction (V2V), and Text-to-Video (T2V). The loss is cross-entropy.

![Loss](https://andlukyane.com/images/paper_reviews/nuwa/2021-11-25_17-21-49.jpg)

## Experiments

NÜWA is pre-trained on three datasets:
* Conceptual Captions for text-to-image (T2I) generation. 2.9M text-image pairs
* Moments in Time for video prediction (V2V). 727K videos;
* VATEX for text-to-video (T2V) generation. 241K text-video pairs;

3D representation sizes:

* Text: 1 × 1 × 77 × 1280;
* Image: 21 × 21 × 1 × 1280;
* Video: 21 × 21 × 10 × 1280. 10 frames are sampled from a video with 2.5 fps;

The model is pre-trained on 64 A100 GPU for two weeks.

![Implementation](https://andlukyane.com/images/paper_reviews/nuwa/2021-11-25_17-51-07.jpg)

## Results

<div class="gallery" data-columns="4">
	<img src="{{site.url}}images/paper_reviews/nuwa/2021-11-25_17-34-18.jpg">
	<img src="{{site.url}}images/paper_reviews/nuwa/2021-11-25_17-36-23.jpg">
	<img src="{{site.url}}images/paper_reviews/nuwa/2021-11-25_17-36-40.jpg">
	<img src="{{site.url}}images/paper_reviews/nuwa/2021-11-25_17-51-58.jpg">
</div>

<div class="gallery" data-columns="1">
	<img src="{{site.url}}images/paper_reviews/nuwa/2021-11-25_17-38-22.jpg">
	<img src="{{site.url}}images/paper_reviews/nuwa/2021-11-25_17-38-41.jpg">
	<img src="{{site.url}}images/paper_reviews/nuwa/2021-11-25_17-39-15.jpg">
	<img src="{{site.url}}images/paper_reviews/nuwa/2021-11-25_17-39-35.jpg">
	<img src="{{site.url}}images/paper_reviews/nuwa/2021-11-25_17-40-08.jpg">
</div>

## Ablation study

<div class="gallery" data-columns="4">
	<img src="{{site.url}}images/paper_reviews/nuwa/2021-11-25_17-41-09.jpg">
	<img src="{{site.url}}images/paper_reviews/nuwa/2021-11-25_17-41-29.jpg">
	<img src="{{site.url}}images/paper_reviews/nuwa/2021-11-25_17-41-52.jpg">
	<img src="{{site.url}}images/paper_reviews/nuwa/2021-11-25_17-42-11.jpg">
</div>

* VQ-GAN is better than VQ-VAE;
* increasing the number of discrete tokens improves the performance at the cost of more computing, so 21 is a trade-off version;
* training on all three objectives is better than using only one or two of them;
* nearby attention is better than full attention;
 
## Examples of generated samples

<div class="gallery" data-columns="1">
	<img src="{{site.url}}images/paper_reviews/nuwa/2021-11-25_17-52-58.jpg">
	<img src="{{site.url}}images/paper_reviews/nuwa/2021-11-25_17-53-34.jpg">
	<img src="{{site.url}}images/paper_reviews/nuwa/2021-11-25_17-54-03.jpg">
	<img src="{{site.url}}images/paper_reviews/nuwa/2021-11-25_17-54-28.jpg">
</div>
