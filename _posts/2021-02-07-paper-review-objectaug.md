---
title: 'Paper Review: ObjectAug: Object-level Data Augmentation for Semantic Image Segmentation '
date: 2021-02-07 00:00:00
description: My review of the paper ObjectAug Object-level Data Augmentation for Semantic Image Segmentation .
featured_image: 'https://andlukyane.com/images/paper_reviews/objectaug/image.png'
tags: paperreview deeplearning augmentation imageinpainting imagesegmentation
---

## Paper Review: ObjectAug: Object-level Data Augmentation for Semantic Image Segmentation 

[Paper link](https://arxiv.org/pdf/2102.00221.pdf)

![Main image](https://andlukyane.com/images/paper_reviews/objectaug/image.png)

The idea is quite simple but cool. Surprisingly, such an approach wasn't published before.

The gist of it:
* Take an image with a mask.
* Detach a masked object.
* Fill in the hole with image inpainting.
* Augment the masked object and put it back onto the background.

Thanks to the fact that we have separate objects, we can apply different augmentations to different categories. And we can also augment the background image separately from our objects or add augmentation on the newly formed image.

-----

The approach was tested on PASCAL VOC 2012, Cityscapes, and CRAG.

<div class="gallery" data-columns="4">
	<img src="{{site.url}}images/paper_reviews/objectaug/image_1.png">
	<img src="{{site.url}}images/paper_reviews/objectaug/image_2.png">
	<img src="{{site.url}}images/paper_reviews/objectaug/image_3.png">
	<img src="{{site.url}}images/paper_reviews/objectaug/image_4.png">
</div>

One of the interesting things they checked was the performance of the approach for rare and hard categories.

![The comparison]({{site.url}}images/paper_reviews/objectaug/image_5.png)

And there are a lot of ablation studies.

<div class="gallery" data-columns="4">
	<img src="{{site.url}}images/paper_reviews/objectaug/image_6.png">
	<img src="{{site.url}}images/paper_reviews/objectaug/image_7.png">
	<img src="{{site.url}}images/paper_reviews/objectaug/image_8.png">
	<img src="{{site.url}}images/paper_reviews/objectaug/image_9.png">
</div>