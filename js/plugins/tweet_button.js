function getSelectionText() {
    var text = "";
    if (window.getSelection) {
        text = window.getSelection().toString();
    } else if (document.selection && document.selection.type != "Control") {
        text = document.selection.createRange().text;
    }
    return text;
}
$(document).ready(function (){
   $('div,p').mouseup(function (e){
       if (getSelectionText() != "")
       {
       window.open('https://twitter.com/intent/tweet?text='+encodeURI(getSelectionText())+'&url='+encodeURI(document.URL));
       }
   })
});
